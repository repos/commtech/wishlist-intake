import vue from 'rollup-plugin-vue';
import copy from 'rollup-plugin-copy';
import json from '@rollup/plugin-json';
import less from 'rollup-plugin-less';
import config from './src/common/config.js';

// Include a banner comment at the top of each generated file.
const getBanner = ( gadgetName, gadget ) => {
	return '/**\n' +
		' * ' + gadgetName + ': ' + gadget.description + '\n' +
		' * From [[Community Tech]]\n' +
		' * Compiled from source at https://gitlab.wikimedia.org/repos/commtech/wishlist-intake\n' +
		' * Please submit code changes as a merge request to the source repository.\n' +
		' */\n';
};

/**
 * Get the configuration for the copy plugin for the given gadget.
 * @param {string} gadget
 * @return {Object} CopyOptions
 */
const getCopyConfig = ( gadget ) => {
	return {
		targets: [ {
			src: `dist/${ gadget }.js`,
			dest: 'dist',
			// This makes the build usable as a user script. It is not needed for the
			// production build, but is harmless to include, so we do so for simplicity.
			transform: ( contents ) => {
				return '// <nowiki>\n' +
					`mw.loader.using( ${ JSON.stringify( config.gadgets[ gadget ].dependencies ) } ).then( ` +
					`( require ) => {\n${ contents }\n} );\n// </nowiki>`;
			}
		} ],
		hook: 'writeBundle'
	};
};

export default [
	// WishlistIntake
	{
		input: {
			WishlistIntake: 'src/webapp/init.js'
		},
		output: {
			dir: 'dist',
			format: 'cjs',
			banner: getBanner( 'WishlistIntake', config.gadgets.WishlistIntake )
		},
		plugins: [
			vue(),
			json(),
			less( {
				output: false,
				insert: true
			} ),
			copy( getCopyConfig( 'WishlistIntake' ) )
		],
		external: [ 'vue', '@wikimedia/codex' ]
	},
	// WishlistManager
	{
		input: {
			WishlistManager: 'src/webapp/wishlistManager.init.js'
		},
		output: {
			dir: 'dist',
			format: 'cjs',
			banner: getBanner( 'WishlistManager', config.gadgets.WishlistManager )
		},
		plugins: [
			json(),
			copy( getCopyConfig( 'WishlistManager' ) )
		]
	},
	// WishlistTranslation
	{
		input: {
			WishlistTranslation: 'src/webapp/wishlistTranslation.init.js'
		},
		output: {
			dir: 'dist',
			format: 'cjs',
			banner: getBanner( 'WishlistTranslation', config.gadgets.WishlistTranslation )
		},
		plugins: [
			vue(),
			json(),
			less( {
				output: false,
				insert: true
			} ),
			copy( getCopyConfig( 'WishlistTranslation' ) )
		],
		external: [ 'vue', '@wikimedia/codex', 'mediawiki.storage' ]
	}
];
