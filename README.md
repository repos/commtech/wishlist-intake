# Wishlist Intake

A gadget for intake and editing of Wishlist proposals.

## Contributing

For Node dependencies, you can use:
* [Fresh](https://www.mediawiki.org/wiki/Fresh) docker container (recommended)
    * Using Fresh will ensure your Node and NPM dependencies are
      the same as the CI environment and other contributors.
    * However, the local server and deploy scripts currently can't
      be used inside of Fresh (yet).
* [Node](https://nodejs.org/) of the version specified by [.nvmrc](.nvmrc)

### MediaWiki requirements

To fully test the Community Wishlist experience on a local MediaWiki installation,
you'll need the following extensions installed:

* [Extension:Cite](https://www.mediawiki.org/wiki/Extension:Cite)
* [Extension:Citoid](https://www.mediawiki.org/wiki/Extension:Citoid)
* [Extension:DiscussionTools](https://www.mediawiki.org/wiki/Extension:DiscussionTools) (optional)
  * Not required, but recommended for a better discussion experience.
* [Extension:Gadgets](https://www.mediawiki.org/wiki/Extension:Gadgets)
* [Extension:InputBox](https://www.mediawiki.org/wiki/Extension:InputBox)
* [Extension:ParserFunctions](https://www.mediawiki.org/wiki/Extension:ParserFunctions)
* [Extension:Scribunto](https://www.mediawiki.org/wiki/Extension:Scribunto)
* [Extension:TemplateStyles](https://www.mediawiki.org/wiki/Extension:TemplateStyles)
* [Extension:Translate](https://www.mediawiki.org/wiki/Extension:Translate)
  * The [installation instructions](https://www.mediawiki.org/wiki/Help:Extension:Translate/Installation)
    are hard to follow. Start by downloading the extension as well as
    [UniversalLanguageSelector](https://www.mediawiki.org/wiki/Extension:UniversalLanguageSelector).
  * Add the following to your `LocalSettings.php`:
    ```
    wfLoadExtension( 'Translate' );
    $wgGroupPermissions['user']['translate'] = true;
    $wgGroupPermissions['user']['translate-messagereview'] = true;
    $wgGroupPermissions['user']['translate-groupreview'] = true;
    $wgGroupPermissions['user']['translate-import'] = true;
    $wgGroupPermissions['sysop']['pagetranslation'] = true;
    $wgGroupPermissions['sysop']['translate-manage'] = true;
    $wgTranslateDocumentationLanguageCode = 'qqq';
    $wgExtraLanguageNames['qqq'] = 'Message documentation';
    wfLoadExtension( 'UniversalLanguageSelector' );
    ```
  * Run `maintenance/run update` to update the database.
* [Extension:VisualEditor](https://www.mediawiki.org/wiki/Extension:VisualEditor)
* [Extension:WikimediaMessages](https://www.mediawiki.org/wiki/Extension:WikimediaMessages)
  (optional)
  * This provides the localized names for projects, but is otherwise optional to install.

You'll also want to enable [InstantCommons](https://www.mediawiki.org/wiki/Manual:$wgUseInstantCommons)
by adding `$wgUseInstantCommons = true;` to your LocalSettings.php.

### Quickstart

1. `npm install` to install dependencies
2. `npm run setup` to deploy the gadget and survey templates on the configured wiki
   (see the [Deployment](#Deployment) section below for more)
    * If you want to start with a clean slate, you can run `npm run setup:nuke`
      to delete all the pages created by the setup script.
    * You can use `npm run setup:demo` to deploy dummy data for testing.
    * Run `npm run setup:user-group-css` to deploy the user group CSS
      (the normal setup script will complain if this needs to run).
    * The setup script will tell you if you need to update the `MediaWiki:Gadgets-definition` page.
3. `npm run server` to start a development server (can't be used in Fresh)
4. `npm run watch` to watch for changes and rebuild

**On-wiki tasks (only need to be done once):**

* Browse to `Special:Tags` on your wiki and create a tag with the name `community-wishlist`.
  This tag is applied to edits made using the intake form.
* Browse to `Special:AbuseFilter/import` and create the following filters:
    * [Disallow non-staff from changing restricted fields](src/mediawiki-pages/abuse-filters/disallowNonStaffFromChangingRestrictedFields.json)
    * [Enforce values of enum fields](src/mediawiki-pages/abuse-filters/enforceValuesOfEnumFields.json)

Next we need to load the dev build from the wiki. For development, it's recommended to
use your local MediaWiki installation. This way you can create proposals without needing
to have the test pages deleted by volunteer admins.

On your local wiki, add the following to `Special:MyPage/common.js`:
```javascript
// Use the gadget if it's enabled, otherwise load the development build.
if ( mw.loader.getState( 'ext.gadget.WishlistIntake' ) !== 'loaded' ) {
	mw.loader.load( 'http://localhost:5501/dist/WishlistIntake.js' );
	mw.loader.load( 'http://localhost:5501/dist/WishlistManager.js' );
}
```

The user entrypoint is at `Community_Wishlist` which serves as the dashboard.
The intake form can be found at `Community_Wishlist/Intake`,
and existing wishes can be edited using the "Edit with form" tab.

**Before committing**, be sure to run `npm run build` to create a production build.

### Testing

* `npm test` to run linting and unit tests
* `npm run fix` to automatically fix linting issues and re-run tests
* `npm run selenium-test` to run Selenium tests (see below).

#### Selenium tests

To run the Selenium tests, you'll need credentials for an admin account on a local
MediaWiki installation. See [mediawiki.org](https://www.mediawiki.org/wiki/Selenium) for
general information on running Selenium tests against MediaWiki.

The credentials for this repo's Selenium tests can be passed in with the same `MW_` and `MEDIAWIKI_`
[environment variables](https://www.mediawiki.org/wiki/Selenium/How-to/Set_environment_variables)
you use for local wiki, or in an object called `test` in `config/credentials.json`:

```
{
	…
	"test": {
		"server": "http://localhost:8080",
		"scriptPath": "/w",
		"username": "my admin username",
		"password": "my admin password"
	}
}
```

The Selenium test will create actual pages on the wiki, so you should only run this on a
local MediaWiki installation. `username` should be an admin account. Note that the username
and password for `test` are not [BotPasswords](https://www.mediawiki.org/wiki/Manual:Bot_passwords)
like they are for other credentials in this file.

Once you have credentials, run the Selenium tests with `npm run selenium-test`. Any pages
created should be deleted after the tests are run.

### Localization

While this remains a gadget, and [Message Bundles](https://www.mediawiki.org/wiki/Help:Extension:Translate/Message_Bundles)
are still in development, we use a system of a translatable wikitext-based JSON page to
utilize the Translate extension.

To add a new message:
* Add the message to `messages` block in `config/config.json`
* Run `npm run setup:i18n` to update the message page in the code repository.
* Run `npm run deploy` again to update the message page on the wiki.

Other messages such as the localized project names (i.e. "Wikipedia"), we use existing
messages from the WikimediaMessages extension. These are specified in `config/config.json`
in the `importedMessages` block.

## Deployment

You must have `interface-admin` and `sysop` rights to use the deployment script.

1. On your local, login as an admin and add "Interface administrator" for your
   account at `Special:UserRights`.
2. Visit `Special:BotPasswords` to create a bot password with the following grants:

   _For all environments:_
    * "High-volume (bot) access"
    * "Edit existing pages"
    * "Edit protected pages"
    * "Edit the MediaWiki namespace and sitewide/user JSON"
    * "Edit sitewide and user CSS/JS"
    * "Create, edit, and move pages"

   _For local and testing credentials only:_
    * "Delete pages, revisions, and log entries"
3. After obtaining the credentials, run
`cp config/credentials.json.dist config/credentials.json`
and change the details accordingly:
```
{
	"local": {
		"apiUrl": "http://localhost:8080/w/api.php",
		"username": "Exampleuser@somedescription",
		"password": "mybotpassword1234567890123456789"
	},
	"test": {
		…
	}
}
```

`local` is just an arbitrary name given to this set of credentials. You can add more
credentials, for example `staging` for a staging wiki, and use `--credentials=staging`
to deploy to that wiki as with `npm run deploy -- --credentials=staging`.
If the `--credentials` flag is not provided, it defaults to the first set of credentials.

`test` is a special set of credentials used by the Selenium tests. See the [Selenium tests](#selenium-tests)
section for more. If you're not running the Selenium tests (which are only ran loclaly),
you can ignore or remove the `test` credentials.

For production deployments, you may want to provide a custom edit summary, as with
`npm run deploy -- "[edit summary]"`. The edit summary is transformed to include the
version number and git SHA, e.g. "v5.5.5 at abcd1234: [edit summary]". If an edit summary
isn't provided, the latest commit message is used.

The deployment script may have further instructions if the gadget has not yet
been fully installed on the wiki or other actions are needed.

### Deploying individual pages

If you only want to deploy a single page, you can use the `--page` flag with the
`deploy` script. For example, to deploy `Module:Community Wishlist` page, you can run:
```
npm run deploy -- --page="Module:Community Wishlist"
```

### Pulling changes from the wiki

To pull changes from the wiki, you can use the `pull` script. This will update the
local files with the latest version from the wiki.

Pull all pages known to the local repository:
```
npm run pull
```

Pull a specific page:
```
npm run pull -- --page="Module:Community Wishlist"
```

## Index page updates

A bot is responsible for looping through all wishes and maintaining the index pages.
It runs on Node.js and shares code with the gadget.

Use `npm run update-indexes` to run the bot. In production, it will run as a scheduled
job on Toolforge.

## Templates and page structure

### Static templates

The gadget uses a system of templates and modules which are responsible for presentation
of data. These only augment the data given to them, and do not fetch data themselves.

* [Template:Community Wishlist/styles.css](src/mediawiki-pages/Template:Community_Wishlist~styles.css) -
  TemplateStyles for all templates and pages.
* [Template:Community Wishlist/Wish](src/mediawiki-pages/Template:Community_Wishlist~Wish.mediawiki) -
  The main template for a wish.
* [Template:Community Wishlist/Wishes](src/mediawiki-pages/Template:Community_Wishlist~Wishes.mediawiki) -
  A table to show all, recent, or archived wishes. The rows are kept in the bot-maintained templates listed below.
* [Template:Community Wishlist/Wishes/Row](src/mediawiki-pages/Template:Community_Wishlist~Wishes~Row.mediawiki) -
  A row in the index of wishes.
* [Template:Community Wishlist/Focus area](src/mediawiki-pages/Template:Community_Wishlist~Focus_area.mediawiki) -
  The template for a focus area page. This also uses the Row template, above.
* [Template:Community Wishlist/Focus area/Card](src/mediawiki-pages/Template:Community_Wishlist~Focus_area~Card.mediawiki) -
  The template for a focus area overview card. These are used on the focus area index
  and dashboard pages.
* [Template:Community Wishlist/Status](src/mediawiki-pages/Template:Community_Wishlist~Status.mediawiki) -
  The template for a wish or focus area status.
* [Module:Community Wishlist](src/mediawiki-pages/Module:Community_Wishlist.lua) -
  A Lua module for utility functions.

### Bot-maintained templates

These are updated with each run of `npm run update-indexes` and are transcluded in other
pages as well as i18n messages.

* [Template:Community Wishlist/Wishes/All](src/mediawiki-pages/Template:Community_Wishlist~Wishes~All.mediawiki) -
  Contains rows for all wishes.
* [Template:Community Wishlist/Wishes/Recent](src/mediawiki-pages/Template:Community_Wishlist~Wishes~Recent.mediawiki) -
  Contains rows for recent wishes.
* [Template:Community Wishlist/Wishes/Archive](src/mediawiki-pages/Template:Community_Wishlist~Wishes~Archive.mediawiki) -
  Contains rows for archived wishes.
* Template:Community Wishlist/Wishes/<focus-area-slug> -
  These templates contain rows for each focus area's wishes.
  See the [demo one](src/mediawiki-pages/demo-only/Community_Wishlist~Wishes~Auto-save_for_page_content.mediawiki) for example.
* [Template:Community Wishlist/Focus areas](src/mediawiki-pages/Template:Community_Wishlist~Focus_areas.mediawiki) -
  Contains overview cards of all focus areas.
* [Template:Community Wishlist/Focus areas/<focus-area-slug>](src/mediawiki-pages/demo-only/Template:Community_Wishlist~Focus_areas~Improve_search_in_all_Wikimedia_projects.mediawiki) -
  Contains rows for wishes in a focus area.

### Page structure

These pages can be created with `npm run setup`, and `npm run setup:demo` for dummy data.

* [Community Wishlist](src/mediawiki-pages/Community_Wishlist.mediawiki) -
  The main dashboard page.
* [Community Wishlist/Intake](src/mediawiki-pages/Community_Wishlist~Intake.mediawiki) -
  The intake form, used for creating new wishes. Editing existing wish pages is done via a custom URL parameter: `?editwish=1`.
* [Community Wishlist/Wishes](src/mediawiki-pages/Community_Wishlist~Wishes.mediawiki) -
  The index of all wishes.
* [Community Wishlist/Wishes/<wish-slug>](src/mediawiki-pages/demo-only/Community_Wishlist~Wishes~Auto-save_for_page_content.mediawiki) -
  The wish detail page.
* [Community Wishlist/Focus areas](src/mediawiki-pages/Community_Wishlist~Focus_areas.mediawiki) -
  The index of active focus areas.
* [Community Wishlist/Focus areas/<focus-area-slug>](src/mediawiki-pages/demo-only/Community_Wishlist~Focus_areas~Improve_search_in_all_Wikimedia_projects.mediawiki) -
  Focus area page with the index of wishes and the voting section.
