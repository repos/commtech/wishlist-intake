import WishlistIntake from './WishlistIntake.vue';
import FocusArea from './FocusArea.vue';
import WebUtil from '../webapp/WebUtil.js';
import Util from '../common/Util.js';
import Wish from '../common/Wish.js';
import config from '../common/config.js';

const api = new mw.Api();

/**
 * If the page already exists, pre-fetch the content
 * so that it's available when the form is loaded.
 *
 * @return {Promise|jQuery.Promise}
 */
function loadWishData() {
	if ( WebUtil.isNewWish() ) {
		return Promise.resolve();
	}

	return api.get( {
		action: 'query',
		format: 'json',
		prop: 'revisions',
		titles: mw.config.get( 'wgPageName' ),
		rvprop: [ 'content', 'timestamp' ],
		rvslots: 'main',
		formatversion: 2,
		assert: 'user',
		curtimestamp: true
	} ).then( ( res ) => {
		const page = res.query && res.query.pages ? res.query.pages[ 0 ] : {};
		if ( page.missing ) {
			// TODO: show button to create wish and pre-fill the title with the subpage name.
			return {};
		}
		const revision = page.revisions[ 0 ];
		const template = Util.getWishTemplate();
		const wikitext = revision.slots.main.content;
		const wish = template.getWish( wikitext, mw.config.get( 'wgPageName' ) );
		// Confirm that we can parse the wikitext.
		if ( !wish ) {
			return null;
		}
		const wishData = {
			status: wish.status,
			type: wish.type,
			title: wish.title,
			description: wish.description,
			audience: wish.audience,
			tasks: Wish.getArrayFromValue( wish.tasks ),
			proposer: wish.proposer,
			created: wish.created,
			projects: Wish.getArrayFromValue( wish.projects ),
			otherproject: wish.otherproject,
			area: wish.area,
			baselang: wish.baselang
		};
		// Confirm that we can parse and then re-create the same wikitext.
		if ( template.getWikitext( wishData ) !== wikitext ) {
			WebUtil.logError( 'Parsing failed for ', mw.config.get( 'wgPageName' ) );
		}
		return Object.assign( {
			// For edit conflict detection.
			basetimestamp: revision.timestamp,
			curtimestamp: res.curtimestamp
		}, wishData );
	} );
}

/**
 * Show a banner after a wish has been saved.
 */
function showPostEditBanner() {
	// Close image.
	const closeImg = document.createElement( 'img' );
	closeImg.src = 'https://upload.wikimedia.org/wikipedia/commons/8/82/Codex_icon_close.svg';
	closeImg.alt = mw.msg( 'communitywishlist-close' );
	// Close button.
	const closeButton = document.createElement( 'button' );
	closeButton.className = 'cdx-button cdx-button--action-default cdx-button--weight-quiet cdx-button--size-medium cdx-button--icon-only cdx-message__dismiss-button';
	closeButton.ariaLabel = mw.msg( 'communitywishlist-close' );
	// Message icon.
	const messageIcon = document.createElement( 'span' );
	messageIcon.className = 'cdx-message__icon';
	// View wishes link.
	const viewWishesLink = document.createElement( 'a' );
	viewWishesLink.href = mw.util.getUrl( 'Special:MyLanguage/Community Wishlist/Wishes' );
	viewWishesLink.textContent = mw.msg( 'communitywishlist-view-all-wishes' );
	// Message content.
	const messageContent = document.createElement( 'div' );
	messageContent.className = 'cdx-message__content';
	const messageContentMsg = mw.config.get( 'wgPostEdit' ) === 'created' ? 'communitywishlist-create-success' : 'communitywishlist-edit-success';
	// Messages that can be used here:
	// * communitywishlist-create-success
	// * communitywishlist-edit-success
	messageContent.textContent = mw.msg( messageContentMsg ) + ' ';
	// Message container.
	const messageContainer = document.createElement( 'div' );
	messageContainer.className = 'cdx-message cdx-message--block cdx-message--success';
	messageContainer.ariaLive = 'polite';
	// Append elements.
	closeButton.appendChild( closeImg );
	messageContent.appendChild( viewWishesLink );
	messageContainer.appendChild( messageIcon );
	messageContainer.appendChild( messageContent );
	messageContainer.appendChild( closeButton );
	document.querySelector( '.mw-body-content' ).prepend( messageContainer );
	// Close the banner when the close button is clicked.
	closeButton.addEventListener( 'click', () => messageContainer.remove() );
}

/**
 * Show an error message when a wish fails to load.
 *
 * @param {HTMLElement} mwContentText
 */
function handleWishLoadError( mwContentText ) {
	const errorMsg = mw.message( 'communityrequests-wish-loading-error',
		window.location.href,
		`Special:EditPage/${ mw.config.get( 'wgPageName' ) }`,
		'Talk:Community Wishlist'
	);
	mwContentText.prepend(
		WebUtil.getMessageBox( errorMsg, 'error' )
	);
}

/**
 * Entry point for the gadget.
 */
$( () => {
	// In case Category:Community_Wishlist/Intake is on an unexpected page.
	if ( !( WebUtil.isWishRelatedPage() || WebUtil.isFocusAreaPage() ) ) {
		return;
	}

	// Don't load the form on protected pages (T369352).
	// We don't have config.importedMessages yet, so instead of showing an error
	// we'll redirect to action=edit where they can review the source.
	if ( !mw.config.get( 'wgIsProbablyEditable' ) && WebUtil.isWishEdit() ) {
		window.location.replace( mw.util.getUrl( WebUtil.getPageName(), { action: 'edit' } ) );
		return;
	}

	const mwContentText = document.querySelector( '#mw-content-text' );

	// Load all required i18n messages.
	const promises = [
		api.loadMessages( config.importedMessages ),
		WebUtil.setOnWikiMessages( api )
	];

	if ( WebUtil.isWishEdit() ) {
		// Pre-fetch the wish data.
		promises.push( loadWishData() );
	}

	// Load all required i18n messages, and then the rest of the gadget.
	Promise.all( promises ).then( ( res ) => {
		const Vue = require( 'vue' );

		// Load the focus area app if we're on a focus area page.
		if ( WebUtil.isFocusAreaPage() ) {
			const focusAreaApp = document.createElement( 'div' );
			document.querySelector( '.community-wishlist-voting-btn' )
				.closest( 'p' )
				.replaceWith( focusAreaApp );
			Vue.createMwApp( FocusArea ).mount( focusAreaApp );
			return;
		}

		mw.hook( 'postEdit' ).add( showPostEditBanner );

		if ( !WebUtil.shouldShowForm() ) {
			return;
		}

		if ( res[ 0 ] === null ) {
			handleWishLoadError( mwContentText );
			return;
		}
		let wishData = {};
		if ( res[ 2 ] && res[ 2 ].title ) {
			wishData = res[ 2 ];
		}

		const root = document.createElement( 'div' );
		root.className = 'wishlist-intake-container';
		mwContentText.replaceWith( root );
		Vue.createMwApp( WishlistIntake, wishData ).mount( root );
	} );

} );
