import config from '../common/config.js';
import WebUtil from './WebUtil.js';
import Util from '../common/Util.js';
import Wish from '../common/Wish.js';

/**
 * WishlistManager
 *
 * This is used by staff and translation admins to manage wishes. Currently, it's
 * only used for translation preparation, but may later be expanded to include other
 * functionality
 *
 * This is a separate gadget from WishlistIntake because it has to be loaded on
 * Special:PageTranslation as well as action=edit of wish pages, while WishlistIntake
 * only loads on action=view for [[Category:Community_Wishlist/Intake]].
 */
export default class WishlistManager {

	/**
	 * Has the current wish page been set up for translation?
	 *
	 * @return {boolean}
	 */
	static isMarkedForTranslation() {
		return WebUtil.isWishPage() &&
			mw.config.get( 'wgCategories' ).includes( config.wishCategory + '/Translatable' );
	}

	/**
	 * Initialize the WishlistManager gadget.
	 */
	init() {
		if ( WebUtil.isWishPage() ) {
			this.addEditLink();
			this.updateDiscussionLink();
		}

		if ( WebUtil.isWishView() ) {
			// When viewing a wish…
			this.addPrepareForTranslationButton();
		} else if ( WebUtil.isManualWishEdit() && mw.util.getParamValue( 'translationprep' ) ) {
			// When editing a wish manually and the translationprep parameter is set…
			this.addTranslateTags();
		} else if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'PageTranslation' ) {
			// When on Special:PageTranslation…
			this.handleSpecialPageTranslation();
		}
	}

	/**
	 * Add the "Prepare for translation" button next to the "Edit wish" button.
	 * This is disabled on mobile, since we have to check wgCategories to determine
	 * if the wish is marked for translation, and that's not available in MobileFrontend.
	 */
	addPrepareForTranslationButton() {
		if ( !WebUtil.isStaff() || WebUtil.isMobile() ) {
			return;
		}
		const button = document.createElement( 'button' );
		button.className = 'community-wishlist-translation-prep-btn cdx-button';
		if ( WishlistManager.isMarkedForTranslation() ) {
			button.className += ' cdx-button--action-default';
			button.textContent = 'Marked for translation';
			button.onclick = () => {
				window.location.replace(
					mw.util.getUrl(
						`Special:PageTranslation/${ WebUtil.getPageName() }`,
						{ do: 'mark' }
					)
				);
			};
		} else {
			button.className += ' cdx-button--action-progressive cdx-button--weight-primary';
			button.textContent = 'Prepare for translation';
			button.onclick = () => {
				window.location.replace( mw.util.getUrl( WebUtil.getPageName(), { action: 'edit', translationprep: '1' } ) );
			};
		}
		document.querySelector( '.community-wishlist-edit-wish-btn' ).after( button );
	}

	/**
	 * Add <translate> tags to the wish page for appropriate fields.
	 */
	addTranslateTags() {
		const $textarea = $( '#wpTextbox1' );
		const template = Util.getWishTemplate();
		// Get a Wish object from the wikitext.
		const wikitext = $textarea.textSelection( 'getContents' );
		if ( wikitext.includes( '<translate>' ) ) {
			mw.notify(
				'<translate> tags already found on this page. No changes made.',
				{ title: 'Community Wishlist Manager' }
			);
			return;
		}
		const wish = template.getWish( wikitext, WebUtil.getPageName() );
		// Convert back, this time inserting the <translate> tags where needed.
		wish.title = `<translate>${ wish.title }</translate>`;
		wish.description = `<translate>${ wish.description }</translate>`;
		wish.audience = `<translate>${ wish.audience }</translate>`;
		if ( wish.otherproject ) {
			wish.otherproject = `<translate>${ wish.otherproject }</translate>`;
		}
		// Change the status to "Open".
		wish.status = Wish.STATUS_OPEN;
		$textarea.textSelection( 'setContents', template.getWikitext( wish ) );
		this.notifyTranslateTags();
	}

	/**
	 * Notify the user that the tags have been added, with a link to the Staff instructions page.
	 */
	notifyTranslateTags() {
		mw.loader.using( 'mediawiki.jqueryMsg' ).then( () => {
			// Needs to be a mw.Message object to evaluate as wikitext.
			mw.messages.set( {
				'community-wishlist-pre-translation-notify': 'Translate tags added. Please review and add any ' +
					`<code><nowiki><tvar></nowiki></code> syntax as needed. [[${ config.wishHomePage + '/Staff instructions' }|Learn more]].`
			} );
			mw.notify(
				mw.message( 'community-wishlist-pre-translation-notify' ),
				{
					title: 'Community Wishlist Manager',
					autoHideSeconds: 'long'
				}
			);
		} );
	}

	/**
	 * Handle use of Special:PageTranslation.
	 *
	 * If marking a wish page for translation, the "Allow translation of page title"
	 * option is unchecked. This is because the title is already translatable via the
	 * wish page content.
	 */
	handleSpecialPageTranslation() {
		// For the more common query string URL.
		let wishPageTitle = mw.util.getParamValue( 'target' ).replaceAll( '_', ' ' );
		if ( mw.config.get( 'wgTitle' ).includes( '/' ) ) {
			// We're on the path-like URL, i.e. Special:PageTranslation/Page_title
			wishPageTitle = mw.config.get( 'wgTitle' ).replace( /^PageTranslation/, '' );
		}

		if ( !wishPageTitle.startsWith( config.wishPagePrefix ) ) {
			// Either we're at post-submission, or this is not for a wish page.
			return;
		}

		// Disable the "Allow translation of page title" option.
		$( '[name=translatetitle]' ).prop( 'checked', false );
	}

	/**
	 * Show an edit link on proposal subpages (including while editing).
	 */
	addEditLink() {
		if ( WebUtil.isMobile() ) {
			this.addEditLinkMobile();
			return;
		}

		const messages = config.gadgets.WishlistManager.messages;
		( new mw.Api() ).loadMessages( messages ).then( () => {
			const editItem = document.querySelector( '#ca-edit' );
			const editWithFormItem = editItem.cloneNode( true );
			const editWithForm = editWithFormItem.querySelector( 'a' );
			editWithFormItem.id = 'ca-wishlist-intake-edit';
			const pageTitle = WebUtil.isNewWish() ?
				config.wishIntakePage :
				config.wishPagePrefix + WebUtil.getWishSlug();
			editWithForm.href = mw.util.getUrl( pageTitle, { [ config.wishEditParam ]: 1 } );
			delete editWithForm.title;
			delete editWithForm.accesskey;
			// The <a> sometimes contains a <span> and sometime doesn't;
			// we can leave it out because it doesn't seem to change anything.
			editWithForm.textContent = mw.msg( 'communityrequests-edit-with-form' );
			editItem.after( editWithFormItem );
			// Highlight the "Edit with form" tab when editing.
			const selectedNode = document.querySelector( '.mw-portlet-views .selected, .skin-monobook #p-cactions.portlet .selected' );
			if ( selectedNode && ( WebUtil.isWishEdit() || WebUtil.isNewWish() ) ) {
				selectedNode.classList.remove( 'selected' );
				editWithFormItem.classList.add( 'selected' );
			} else {
				editWithFormItem.classList.remove( 'selected' );
			}
		} );
	}

	/**
	 * The normal 'Edit' tab is not on mobile. We'll change the familiar pencil icon
	 * to point to the intake form, which should be acceptable as you can still edit
	 * the full wikitext through page menu > "Edit full page".
	 *
	 * We have to use setTimeout() to get around race conditions with MobileFrontend,
	 * as there are no JS hook that we can rely on. The 500ms is arbitrary but seems
	 * to reliably work. Even if it doesn't, the "Edit wish" button is still available.
	 */
	addEditLinkMobile() {
		setTimeout( () => {
			const editTab = document.querySelector( '#ca-edit' );
			editTab.href = mw.util.getUrl(
				WebUtil.getPageName(),
				{ [ config.wishEditParam ]: 1 }
			);
			$( editTab ).off( 'click.mfeditlink' );
		}, 500 );
	}

	/**
	 * Update "Discussion" link to point to base talk page and not /es, /fr etc.
	 */
	updateDiscussionLink() {
		const talkLink = WebUtil.isMobile() ?
			document.querySelector( 'a[rel="discussion"]' ) :
			document.querySelector( '#ca-talk a' );
		talkLink.href = mw.util.getUrl(
			`Talk:${ config.wishPagePrefix }${ WebUtil.getWishSlug() }`
		);
	}
}
