import config from '../common/config.js';

/**
 * This class loads and saves the focus area voting subpages.
 */
class FocusAreaPage {

	constructor() {
		this.api = new mw.Api();
		this.votesPageName = this.getBasePageName() + '/Votes';
		this.voteCountPageName = this.getBasePageName() + config.focusAreaVoteCountSuffix;
	}

	/**
	 * Get the name of the Translate 'source' page for this page.
	 *
	 * @return {string}
	 */
	getBasePageName() {
		let pageName = mw.config.get( 'wgPageName' );
		if ( mw.config.get( 'wgTranslatePageTranslation' ) === 'translation' ) {
			pageName = pageName.slice( 0, pageName.length - mw.config.get( 'wgPageContentLanguage' ).length - 1 );
		}
		return pageName;
	}

	/**
	 * Load the current page's votes data.
	 *
	 * @return {Promise<string>}
	 */
	loadVotes() {
		return this.api.get( {
			action: 'query',
			titles: this.votesPageName,
			prop: 'revisions',
			rvprop: [ 'content', 'timestamp' ],
			rvslots: 'main',
			curtimestamp: true,
			assert: 'user',
			format: 'json',
			formatversion: 2
		} ).then( ( response ) => {
			const page = response.query && response.query.pages ? response.query.pages[ 0 ] : {};
			if ( page.missing ) {
				return '';
			}
			this.starttimestamp = response.curtimestamp;
			this.basetimestamp = page.revisions[ 0 ].timestamp;
			return page.revisions[ 0 ].slots.main.content;
		} );
	}

	/**
	 * Add a vote template to the votes list, and save the full wikitext
	 * for both the Votes page and the Vote_count page.
	 *
	 * @param {string} votes The votes wikitext.
	 * @param {string} comment The current user's support comment.
	 * @return {Promise}
	 */
	addVote( votes, comment ) {
		if ( this.alreadyVoted( votes ) ) {
			// @todo If already voted, change the timestamp and comment of the existing vote.
			return Promise.resolve();
		}
		// @todo Construct support template wikitext somewhere else.
		const newVote = '{{' + config.supportTemplate +
			' |username=' + mw.config.get( 'wgUserName' ) +
			' |timestamp=' + ( new Date() ).toISOString() +
			' |comment=' + comment.replace( '|', '{{!}}' ) +
			' }}';
		votes = votes.trim() + '\n' + newVote;
		// Count the votes by counting the Support template occurences.
		const escapedTemplateName = mw.util.escapeRegExp( config.supportTemplate );
		const voteCount = votes.match( new RegExp( '\\{\\{' + escapedTemplateName, 'g' ) ).length;
		// Save the votes page.
		return this.api.postWithEditToken( this.api.assertCurrentUser( {
			action: 'edit',
			title: this.votesPageName,
			text: votes,
			formatversion: 2,
			// Tag as having been edited by the gadget.
			tags: [ 'community-wishlist' ],
			// Protect against conflicts
			basetimestamp: this.basetimestamp,
			starttimestamp: this.curtimestamp,
			// Localize errors
			uselang: mw.config.get( 'wgUserLanguage' ),
			errorformat: 'html',
			errorlang: mw.config.get( 'wgUserLanguage' ),
			errorsuselocal: true
		} ) ).then( () => {
			// Update vote count after saving the vote.
			return this.api.postWithEditToken( this.api.assertCurrentUser( {
				action: 'edit',
				title: this.voteCountPageName,
				text: voteCount,
				formatversion: 2,
				starttimestamp: this.curtimestamp,
				// Tag as having been edited by the gadget.
				tags: [ 'community-wishlist' ],
				// Localize errors
				uselang: mw.config.get( 'wgUserLanguage' ),
				errorformat: 'html',
				errorlang: mw.config.get( 'wgUserLanguage' ),
				errorsuselocal: true
			} ) );
		} );
	}

	/**
	 * Check if the current user has already voted.
	 *
	 * @param {string} votesWikitext
	 * @return {boolean}
	 */
	alreadyVoted( votesWikitext ) {
		const escapedUsername = mw.util.escapeRegExp( mw.config.get( 'wgUserName' ) );
		const regex = new RegExp( 'username\\s*=\\s*' + escapedUsername );
		return votesWikitext.match( regex ) !== null;
	}
}

export default FocusAreaPage;
