import WishlistManager from './WishlistManager.js';

/**
 * Entry point for the WishlistManager gadget.
 */
if ( mw.config.get( 'wgIsProbablyEditable' ) ||
	mw.config.get( 'wgCanonicalSpecialPageName' ) === 'PageTranslation'
) {
	mw.loader.using( [ 'mediawiki.util', 'mediawiki.api' ], () => {
		const wishlistManager = new WishlistManager();
		wishlistManager.init();
	} );
}
