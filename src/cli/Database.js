import mysql from 'mysql2/promise';

/**
 * A wrapper around a database connection
 */
export default class Database {
	/**
	 * @callback Logger
	 * @param {string} message
	 */

	/**
	 * @param {mysql.Connection} conn
	 * @param {{debugQueries: boolean}} options
	 * @param {Logger} log
	 */
	constructor( conn, options, log ) {
		this.conn = conn;
		this.debugQueries = !!options.debugQueries;
		this.log = log;
	}

	/**
	 * Run a query
	 *
	 * @param {string} sql
	 * @param {Array} values Prepared statement substitutions
	 * @return {Promise<Object>} An object similar to an array of rows
	 */
	async query( sql, values = [] ) {
		const [ res ] = await this.conn.query( sql, values );
		if ( this.debugQueries && this.log ) {
			this.log( '[D] SQL: ' + mysql.format( sql, values ) );
		}
		return res;
	}

	/**
	 * Close the connection
	 *
	 * @return {Promise<void>}
	 */
	async end() {
		return await this.conn.end();
	}

	/**
	 * @callback TransactionHandler
	 * @return {Promise<void>}
	 */

	/**
	 * Run a callback in the context of a transaction. If the callback throws
	 * an exception, roll back the transaction.
	 *
	 * @param {TransactionHandler} callback
	 * @return {Promise<void>}
	 */
	async transact( callback ) {
		await this.query( 'BEGIN' );
		try {
			await callback();
			await this.query( 'COMMIT' );
		} catch ( e ) {
			await this.query( 'ROLLBACK' );
			throw e;
		}
	}
}
