import BotCommand from './BotCommand.js';
import fs from 'fs';
import config from '../common/config.js';
import mwn from 'mwn';
const { ApiEditResponse } = mwn;

export default class DeployCommand extends BotCommand {
	setup( program ) {
		super.setup( program );
		program
			.description( 'Deploy the gadgets and/or add the wiki pages to the wiki.' )
			.argument( '[summary]', 'The edit summary for the gadget deployment.' )
			.option( '--setup', 'Set up templates and wishlist pages.' )
			.option( '--nuke', 'Delete the pages instead of creating them (only works with --setup)' )
			.option( '--user-group-css', 'Write the user group CSS to the wiki.' )
			.option( '--demo', 'Install dummy wishes and topic areas for testing.' )
			.option( '--page <page-title>', 'Deploy only the file for the specified page title.' );
	}

	async doRun() {
		if ( this.options.page ) {
			await this.saveSinglePage();
			return;
		}
		await this.saveGadgets();
		await this.checkUserGroupCSS();
		await this.checkGadgetDefinitions();
		await this.setupTemplates();
	}

	/**
	 * Save a single page to the wiki.
	 */
	async saveSinglePage() {
		const res = await this.saveFileToPage(
			await this.getFilePathFromPageTitle( this.options.page ),
			this.options.page,
			'Setting up templates and wishlist pages'
		);
		if ( res.translatable && !this.isProduction() ) {
			await this.markForTranslation( this.options.page );
		}
	}

	/**
	 * Save a local file to a wiki page.
	 *
	 * @param {string} file
	 * @param {string} pageTitle
	 * @param {string} summary
	 * @return {ApiEditResponse}
	 */
	async saveFileToPage( file, pageTitle, summary ) {
		const content = await fs.promises.readFile( file, 'utf-8' );
		pageTitle = this.normalizePageTitle( pageTitle );
		process.stdout.write( `Saving to ${ pageTitle }… ` );
		const res = await this.bot.save( pageTitle, content, summary ).catch( ( e ) => {
			process.stdout.write( 'failed\n' );
			throw e;
		} );
		process.stdout.write( 'done\n' );
		// Indicate if the page is translatable so we can later mark it for translation if needed.
		if ( content.includes( '<translate' ) ) {
			res.translatable = true;
		}
		return res;
	}

	/**
	 * Delete a page. Used by the --nuke option.
	 *
	 * @param {string} pageTitle
	 */
	async deletePage( pageTitle ) {
		process.stdout.write( `Deleting ${ pageTitle }… ` );
		let successMsg = 'done\n';
		await this.bot
			.delete( pageTitle, 'Nuking wishlist' )
			.catch( ( e ) => {
				if ( e.code === 'missingtitle' ) {
					successMsg = 'not found\n';
					return;
				}
				process.stdout.write( 'failed\n' );
				throw e;
			} );
		process.stdout.write( successMsg );
	}

	/**
	 * Mark a page for translation.
	 *
	 * @param {string} pageTitle
	 */
	async markForTranslation( pageTitle ) {
		process.stdout.write( `Marking ${ pageTitle } for translation… ` );
		await this.bot.request( {
			action: 'markfortranslation',
			title: pageTitle,
			token: this.bot.csrfToken
		} );
		process.stdout.write( 'done\n' );
	}

	/**
	 * Save the gadgets to the wiki.
	 */
	async saveGadgets() {
		const summary = this.getAutoEditSummary( this.program.args[ 0 ] );
		// Save each gadget file.
		for ( const [ , gadgetConfig ] of Object.entries( config.gadgets ) ) {
			if ( gadgetConfig.filesOnWiki ) {
				continue;
			}
			for ( const file of gadgetConfig.files ) {
				await this.saveFileToPage( `./dist/${ file }`, `MediaWiki:Gadget-${ file }`, summary );
			}
		}
	}

	/**
	 * Check if the user group CSS is in place.
	 */
	async checkUserGroupCSS() {
		if ( this.options.userGroupCss ) {
			await this.writeUserGroupCss();
			return;
		}
		process.stdout.write( 'Checking if necessary user group CSS is in place… ' );
		const userCSS = ( await this.bot.read( 'MediaWiki:Group-user.css' ) ).revisions?.[ 0 ]?.content || '';
		const commonCSS = ( await this.bot.read( 'MediaWiki:Common.css' ) ).revisions?.[ 0 ]?.content || '';

		if ( !userCSS.includes( '.anonymous-show' ) ||
			!userCSS.includes( '.user-show' ) ||
			!commonCSS.includes( '.user-show' )
		) {
			process.stdout.write( 'missing!\n' );
			process.stdout.write( '\x1b[33mUser group CSS does not appear to have been setup. Run ' +
				'`npm run setup:user-group-css` to force saving the correct code (this will not run on production).\x1b[0m\n' );
		} else {
			process.stdout.write( 'done\n' );
		}
	}

	/**
	 * Append content to a page if it exists, otherwise create it.
	 *
	 * @param {string} pageTitle
	 * @param {string} content
	 * @param {string} summary
	 */
	async appendToOrCreatePage( pageTitle, content, summary ) {
		try {
			await this.bot.edit( pageTitle, ( rev ) => {
				return {
					text: ( rev.content || '' ) + content,
					summary: summary
				};
			} );
		} catch ( e ) {
			if ( e.code === 'missingtitle' ) {
				await this.bot.create( pageTitle, content, summary );
			} else {
				throw e;
			}
		}
	}

	/**
	 * Write the user group CSS to the wiki.
	 */
	async writeUserGroupCss() {
		this.assertNotProduction();

		process.stdout.write( 'Writing user group CSS to [[MediaWiki:Group-user.css]]… ' );
		const userCSS = `\n.anonymous-show {
\tdisplay: none !important;
}
div.user-show,
p.user-show {
\tdisplay: block !important;
}
span.user-show,
small.user-show {
\tdisplay: inline !important;
}
table.user-show {
\tdisplay: table !important;
}
li.user-show {
\tdisplay: list-item !important;
}`;
		await this.appendToOrCreatePage( 'MediaWiki:Group-user.css', userCSS, 'Setting up user group CSS' );
		process.stdout.write( 'done\n' );

		process.stdout.write( 'Writing user group CSS to [[MediaWiki:Common.css]]… ' );
		const commonCSS = `\n.user-show {
\tdisplay: none;
}`;
		await this.appendToOrCreatePage( 'MediaWiki:Common.css', commonCSS, 'Setting up user group CSS' );
		process.stdout.write( 'done\n' );
	}

	/**
	 * Check the gadget definitions and report if any need updating.
	 */
	async checkGadgetDefinitions() {
		process.stdout.write( 'Determining if gadget definitions need updating… ' );

		// Fetch contents of current Gadgets-definition page.
		const gadgetsDefinitionContent = ( await this.bot.read( 'MediaWiki:Gadgets-definition' ) )
			.revisions?.[ 0 ]?.content;

		// Generate the new gadget definitions.
		let gadgetDefinitions = '';
		for ( const [ gadgetName, gadgetConfig ] of Object.entries( config.gadgets ) ) {
			gadgetDefinitions += this.getGadgetDefinition( gadgetName, gadgetConfig );
		}

		// Compare with the current definition and save if different.
		if ( gadgetsDefinitionContent && gadgetsDefinitionContent.includes( gadgetDefinitions ) ) {
			process.stdout.write( 'no changes needed\n' );
		} else {
			process.stdout.write( 'outdated!\n' );
			console.warn( '\x1b[33mThe expected gadget definitions are missing or have changed and require manual updating.' );
			console.log( `Add the following lines to [[MediaWiki:Gadgets-definition]]:\n\n${ gadgetDefinitions }\x1b[0m\n` );
		}
	}

	/**
	 * Get the definition for a gadget based on the given configuration.
	 *
	 * @param {string} gadgetName
	 * @param {Object} gadgetConfig
	 * @return {string}
	 */
	getGadgetDefinition( gadgetName, gadgetConfig ) {
		const args = [
			gadgetConfig.ResourceLoader ? 'ResourceLoader' : null,
			gadgetConfig.default ? 'default' : null,
			// Don't hide for dev builds so that gadget can be disabled
			// and tested via loading the dist/ scripts directly.
			// Peer gadgets are always hidden.
			this.isProduction() || gadgetConfig.peer ? 'hidden' : null,
			gadgetConfig.package ? 'package' : null
		];
		// Add args with comma-separated values.
		[ 'rights', 'categories', 'namespaces', 'skins', 'dependencies', 'peers' ]
			.forEach( ( key ) => {
				args.push( gadgetConfig[ key ] ? `${ key }=${ gadgetConfig[ key ].join( ',' ) }` : null );
			} );
		return `* ${ gadgetName }[${ args.filter( Boolean ).join( '|' ) }]|${ gadgetConfig.files.join( '|' ) }\n`;
	}

	/**
	 * Check if the bot is running on a production wiki.
	 *
	 * @return {boolean}
	 */
	isProduction() {
		if ( !this.credentials.apiUrl.startsWith( 'https://' ) ) {
			return false;
		}
		const url = new URL( this.credentials.apiUrl );
		const testDomains = [
			'patchdemo.wmflabs.org',
			'patchdemo.wmcloud.org',
			'wishlist-test.toolforge.org'
		];
		return !testDomains.includes( url.hostname );
	}

	/**
	 * Check if the bot is running on a production wiki, and exit if it is.
	 * This to safeguard against accidentally overwriting sensitive pages on a production wiki.
	 */
	assertNotProduction() {
		if ( this.isProduction() ) {
			console.error( '\x1b[31mTemplates, wishlist pages and sitewide CSS should be created manually on production wikis.\x1b[0m' );
			// eslint-disable-next-line n/no-process-exit
			process.exit( 1 );
		}
	}

	/**
	 * Set up templates and wishlist pages.
	 */
	async setupTemplates() {
		if ( !this.options.setup ) {
			return;
		}

		this.assertNotProduction();

		// Wishlist templates and pages.
		const dir = './src/mediawiki-pages/';
		for await ( const file of this.getFiles( dir ) ) {
			// Skip demo files unless the demo option is used, or we're nuking the pages.
			if ( file.includes( '/demo-only/' ) && !this.options.demo && !this.options.nuke ) {
				continue;
			}

			const pageTitle = this.normalizePageTitle( file.split( '/' ).pop() );
			if ( pageTitle === 'README' || file.includes( 'abuse-filters/' ) ) {
				continue;
			}

			if ( this.options.nuke ) {
				await this.deletePage( pageTitle );
				continue;
			}

			const res = await this.saveFileToPage( file, pageTitle, 'Setting up templates and wishlist pages' );

			// Check if the page needs to be marked for translation, and do so if not in production.
			if ( res.translatable && !this.isProduction() ) {
				await this.markForTranslation( pageTitle );
			}

			// Set content language for MediaWiki:Lang pages.
			if ( pageTitle.startsWith( 'MediaWiki:Lang/' ) ) {
				const lang = pageTitle.replace( 'MediaWiki:Lang/', '' );
				await this.bot.request( {
					action: 'setpagelanguage',
					title: pageTitle,
					// en-rtl is not a real language, so we use a real RTL language.
					lang: lang === 'en-rtl' ? 'ar' : lang,
					token: await this.bot.getCsrfToken()
				} ).catch( ( e ) => {
					if ( e.code === 'pagelang-unchanged-language' ) {
						return;
					}
					throw e;
				} );
			}
		}
	}

}
