import Database from './Database.js';
import MetaDataStore from './MetaDataStore.js';
import Wish from '../common/Wish.js';

const DAY = 86400 * 1000;
const CACHE_EXPIRY = 7 * DAY;
const CACHE_VERSION = 1;

/**
 * A persistent cache of the wish page contents
 */
export default class WishStore {
	/**
	 * @callback Logger
	 * @param {string} message
	 */

	/**
	 * @param {Database} db
	 * @param {MetaDataStore} metaDataStore
	 * @param {Logger} log
	 */
	constructor( db, metaDataStore, log ) {
		this.db = db;
		this.metaDataStore = metaDataStore;
		this.log = log;
	}

	/**
	 * Determine whether the cache needs to be rebuilt from scratch
	 *
	 * @return {Promise<boolean>}
	 */
	async needsRebuild() {
		const meta = await this.metaDataStore.getAll();
		if ( meta.wishVersion !== CACHE_VERSION ) {
			// Version has changed
			return true;
		}
		if ( Date.now() - meta.wishCacheTime > CACHE_EXPIRY ) {
			// The recentchanges table may not be reliable after such a long time
			return true;
		}
		return false;
	}

	/**
	 * Determine whether the index pages need to be regenerated from the cache
	 *
	 * @return {Promise<boolean>}
	 */
	async needsIndexUpdate() {
		const meta = await this.metaDataStore.getAll();
		return !meta.wishCacheTime || !meta.wishIndexTime ||
			meta.wishCacheTime.getTime() !== meta.wishIndexTime.getTime();
	}

	/**
	 * Get all wishes in the cache as an async iterator
	 *
	 * @yield {Wish}
	 */
	async *getWishes() {
		const res = await this.db.query( 'SELECT * FROM wish ORDER BY wish_updated_dt DESC' );
		for ( const row of res ) {
			yield new Wish( {
				pageId: row.wish_page_id,
				page: row.wish_page,
				lang: row.wish_lang,
				updated: row.wish_updated_dt.toISOString(),
				baselang: row.wish_base_lang,
				name: row.wish_name,
				type: row.wish_type,
				status: row.wish_status,
				title: row.wish_title,
				description: row.wish_description,
				audience: row.wish_audience,
				tasks: row.wish_tasks,
				proposer: row.wish_proposer,
				created: row.wish_created,
				projects: row.wish_projects,
				otherproject: row.wish_other_project,
				area: row.wish_area
			} );
		}
	}

	/**
	 * Remove all entries from the cache
	 *
	 * @return {Promise<void>}
	 */
	async truncate() {
		await this.db.query( 'DELETE FROM wish' );
	}

	/**
	 * Insert a wish into the database
	 *
	 * @param {Wish} wish
	 * @return {Promise<void>}
	 */
	async insertWish( wish ) {
		/* eslint-disable camelcase */
		const row = {
			wish_page_id: wish.pageId,
			wish_page: wish.page,
			wish_name: wish.name,
			wish_lang: wish.lang,
			wish_updated_dt: this.parseApiDate( wish.updated, wish.page ),
			wish_base_lang: wish.baselang,
			wish_type: wish.type,
			wish_status: wish.status,
			wish_title: wish.title,
			wish_description: wish.description,
			wish_audience: wish.audience,
			wish_tasks: wish.tasks,
			wish_proposer: wish.proposer,
			wish_created: wish.created,
			wish_created_dt: this.parseTemplateDate( wish.created, wish.page ),
			wish_projects: wish.projects,
			wish_other_project: wish.otherproject,
			wish_area: wish.area
		};
		/* eslint-enable */
		const fields = [];
		const valuePlaceholders = [];
		const values = [];
		for ( const field in row ) {
			fields.push( field );
			valuePlaceholders.push( '?' );
			values.push( row[ field ] );
		}
		await this.db.query(
			'INSERT INTO wish ' +
				'(' + fields.join( ',' ) + ') ' +
				' VALUES (' + valuePlaceholders.join( ',' ) + ')',
			values
		);
	}

	/**
	 * Convert a date string from the template to a Date object
	 *
	 * @param {string} dateString
	 * @param {string} page
	 * @return {Date}
	 */
	parseTemplateDate( dateString, page ) {
		const m = dateString.match(
			/^([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})$/ );
		let date;
		if ( m ) {
			date = new Date( `${ m[ 0 ] }-${ m[ 1 ] }-${ m[ 2 ] }T${ m[ 3 ] }:${ m[ 4 ] }:${ m[ 5 ] }` );
		} else {
			// Remove timezone and comma
			dateString = dateString.replace( ' (UTC)', '' );
			dateString = dateString.replace( ',', '' );
			date = new Date( dateString );
		}
		if ( isNaN( date.getTime() ) ) {
			this.log( `[W] Invalid date "${ dateString }" in page [[${ page }]]` );
			date = new Date();
		}
		return date;
	}

	/**
	 * Convert a date string from an API response to a Date object
	 *
	 * @param {string} dateString
	 * @param {string} page
	 * @return {Date}
	 */
	parseApiDate( dateString, page ) {
		let date = new Date( dateString );
		if ( isNaN( date.getTime() ) ) {
			this.log( `[W] Invalid date "${ dateString }" in page [[${ page }]]` );
			date = new Date();
		}
		return date;
	}

	/**
	 * Set the cache update time. This is typically the revision timestamp of
	 * the last edit to a wish page.
	 *
	 * @param {Date} date
	 * @return {Promise<void>}
	 */
	async setCacheTime( date ) {
		await this.metaDataStore.set( {
			wishVersion: CACHE_VERSION,
			wishCacheTime: date
		} );
	}

	/**
	 * Get the cache update time
	 *
	 * @return {Promise<?Date>}
	 */
	async getCacheTime() {
		return ( await this.metaDataStore.getAll() ).wishCacheTime || null;
	}

	/**
	 * Set the index update time. When an index update is done, this is set to
	 * the cache time, so that a future update to the cache will cause the
	 * index to be invalidated.
	 *
	 * @param {Date} date
	 * @return {Promise<void>}
	 */
	async setIndexUpdateTime( date ) {
		await this.metaDataStore.set( { wishIndexTime: date } );
	}

	/**
	 * Delete a wish given its page_id
	 *
	 * @param {number} pageId
	 * @return {Promise<void>}
	 */
	async deleteWishByPageId( pageId ) {
		await this.db.query( 'DELETE FROM wish WHERE wish_page_id=?', [ pageId ] );
	}

	/**
	 * Insert a wish into the DB, or if it already exists in the cache, update the row.
	 *
	 * @param {Wish} wish
	 * @return {Promise<void>}
	 */
	async upsertWish( wish ) {
		await this.deleteWishByPageId( wish.pageId );
		await this.insertWish( wish );
	}
}
