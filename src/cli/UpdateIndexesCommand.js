import fs from 'fs';

import BotCommand from './BotCommand.js';
import MetaDataStore from './MetaDataStore.js';
import { Mwn } from 'mwn';
import RcFeed from './RcFeed.js';
import Wish from '../common/Wish.js';
import WishStore from './WishStore.js';
import WishStoreUpdater from './WishStoreUpdater.js';
import { setTimeout } from 'node:timers/promises';
import config from '../common/config.js';
import PageLangUpdater from './PageLangUpdater.js';

export default class UpdateIndexesCommand extends BotCommand {
	setup( program ) {
		super.setup( program );
		program
			.description( 'Loops through newly changed wishes and updates the index pages' )
			.option( '--rebuild', 'Rebuild the cache from scratch' )
			.option( '--wait', 'Stay running and poll for new events' );
	}

	async doRun() {
		const db = await this.getDB();
		const metaDataStore = new MetaDataStore( db );
		const wishStore = new WishStore( db, metaDataStore, Mwn.log );
		const rcFeed = new RcFeed( db, metaDataStore, this.bot, Mwn.log );
		const pageLangUpdater = new PageLangUpdater( this.bot, Mwn.log );
		const updater = new WishStoreUpdater(
			db, wishStore, rcFeed, pageLangUpdater, this.bot, Mwn.log );

		if ( this.options.rebuild && this.options.wait ) {
			console.log( "Can't use --rebuild with --wait" );
			return;
		}
		if ( this.options.rebuild || await wishStore.needsRebuild() ) {
			const startTime = new Date();
			await updater.rebuild();
			await rcFeed.setStartTime( startTime );
		}
		while ( true ) {
			await updater.update();
			await pageLangUpdater.update();
			if ( await wishStore.needsIndexUpdate() ) {
				await this.doIndexUpdate( wishStore );
			} else if ( !this.options.wait ) {
				this.log( '[I] Index is already up to date' );
			}

			if ( !this.options.wait ) {
				return;
			}

			await setTimeout( 10000 );
		}
	}

	getLangFallback( values, lang, baseLang ) {
		if ( values[ lang ] ) {
			return values[ lang ];
		}
		if ( values[ baseLang ] ) {
			return values[ baseLang ];
		}
		if ( values.en ) {
			return values.en;
		}
		return Object.values( values )[ 0 ];
	}

	/**
	 * Update all index pages using data from the DB cache
	 *
	 * @param {WishStore} wishStore
	 * @return {Promise<void>}
	 */
	async doIndexUpdate( wishStore ) {
		const cacheTime = await wishStore.getCacheTime();
		if ( !cacheTime ) {
			this.log( "[W] Can't update because cache time is missing" );
			return;
		}

		const wishes = {};
		const allLangs = { en: true };
		const baseLangs = {};

		// Compile wishes by language
		for await ( const wish of wishStore.getWishes() ) {
			const lang = wish.lang;
			baseLangs[ wish.name ] = wish.baselang || 'en';
			wishes[ wish.name ] = wishes[ wish.name ] || {};
			wishes[ wish.name ][ lang ] = wish;
			if ( lang !== '' ) {
				allLangs[ lang ] = true;
			}
		}

		const sortedNames = Object.keys( wishes );
		sortedNames.sort( ( name1, name2 ) => {
			const wish1 = wishes[ name1 ][ '' ] || Object.values( wishes[ name1 ] )[ 0 ];
			const wish2 = wishes[ name2 ][ '' ] || Object.values( wishes[ name2 ] )[ 0 ];
			if ( wish1.updated > wish2.updated ) {
				return -1;
			} else if ( wish1.updated < wish2.updated ) {
				return 1;
			} else {
				return 0;
			}
		} );

		// Build list of indexes
		const indexes = { all: {}, archive: {}, recent: {} };
		for ( const name of sortedNames ) {
			for ( const lang in allLangs ) {
				const baseLang = baseLangs[ name ];
				const wish = this.getLangFallback( wishes[ name ], lang, baseLang );
				const baseWish = wishes[ name ][ '' ] || wish;
				wish.rowText = wish.rowText || ( '| ' + this.getWishIndexRow(
					wish,
					baseLang === wish.lang ? wishes[ name ][ '' ]?.page : null,
					baseWish.updated
				) );
				const rowText = wish.rowText;

				// Archived wishes are only in the archive area
				if ( wish.status === 'archived' ) {
					indexes.archive[ lang ] = indexes.archive[ lang ] || [];
					indexes.archive[ lang ].push( rowText );
					continue;
				}

				// Add row to focus area
				const focusArea = baseWish.area;
				if ( focusArea && focusArea !== 'all' ) {
					indexes[ focusArea ] = indexes[ focusArea ] || {};
					indexes[ focusArea ][ lang ] = indexes[ focusArea ][ lang ] || [];
					indexes[ focusArea ][ lang ].push( rowText );
				}

				// Add row to "all" index
				indexes.all[ lang ] = indexes.all[ lang ] || [];
				indexes.all[ lang ].push( rowText );

				// Add row to "recent" template
				indexes.recent[ lang ] = indexes.recent[ lang ] || [];
				if ( indexes.recent[ lang ].length < this.config.maxRecentWishes ) {
					indexes.recent[ lang ].push( rowText );
				}
			}
		}

		this.log( `[I] Updating indexes: ${ Object.keys( indexes ).join( ', ' ) }` );

		for ( const area in indexes ) {
			for ( const lang in indexes[ area ] ) {
				const rows = indexes[ area ][ lang ].join( '\n' );
				const title = this.getWishIndexTitle( area, lang );
				let response;
				try {
					response = await this.editIndexPage( title, rows );
				} catch ( e ) {
					if ( e instanceof Mwn.Error.MissingPage ) {
						response = await this.createIndexPage( title, rows );
					} else {
						throw e;
					}
				}
				if ( response.result === 'aborted' ) {
					this.log( `[I] Index update aborted [[${ title }]]` );
				} else if ( response.nochange ) {
					this.log( `[I] No change to index [[${ title }]]` );
				} else if ( response.new ) {
					this.log( `[S] Created index [[${ title }]]` );
				} else if ( response.newrevid ) {
					this.log( `[S] Updated index [[${ title }]]` );
				} else {
					this.log( `[W] No new rev_id, something went wrong? [[${ title }]]` );
				}
			}
		}
		await wishStore.setIndexUpdateTime( cacheTime );
		this.log( '[S] Index update complete' );
	}

	/**
	 * Edit an index page
	 *
	 * @param {string} title The page title
	 * @param {string} rows All table rows in combined wikitext format. The
	 *   existing table rows will be removed and replaced with these rows.
	 * @return {Promise<Object>}
	 */
	async editIndexPage( title, rows ) {
		let unchanged = false;
		const result = await this.bot.edit(
			title,
			( rev ) => {
				const oldContent = rev.content;
				const content = this.replaceIndexContent( title, oldContent, rows );
				if ( content === null ) {
					return null;
				}
				if ( content === oldContent ) {
					unchanged = true;
					return null;
				}
				return {
					text: content,
					summary: 'Updating the list of wishes'
				};
			},
			{
				suppressNochangeWarning: true
			}
		);
		return unchanged ? { nochange: true } : result;
	}

	/**
	 * Create an index page
	 *
	 * @param {string} title The page title
	 * @param {string} rows All table rows in wikitext format
	 * @return {Promise<Object>}
	 */
	async createIndexPage( title, rows ) {
		const oldContent = fs.readFileSync(
			this.baseDir + '/src/mediawiki-pages/Template:Community_Wishlist~Wishes~All.mediawiki',
			'utf-8'
		);
		const content = this.replaceIndexContent( title, oldContent, rows );
		if ( content === null ) {
			return { result: 'aborted' };
		}
		return await this.bot.create( title, content );
	}

	/**
	 * Replace all rows in the specified index with a new set of rows
	 *
	 * @param {string} title
	 * @param {string} oldContent
	 * @param {string} rows
	 * @return {?string}
	 */
	replaceIndexContent( title, oldContent, rows ) {
		const startMatch = oldContent.match( /^.*?<!-- wishlist-intake-start.*?-->/s );
		if ( !startMatch ) {
			this.log( `[W] Start marker not found in index page ${ title }` );
			return null;
		}
		const markerEndPos = startMatch[ 0 ].length;

		const endMatch = oldContent.match( /^(.*)<!-- wishlist-intake-end/s );
		let suffixPos;
		if ( !endMatch ) {
			this.log( `[W] End marker not found in index page ${ title }` );
			suffixPos = oldContent.lastIndexOf( '}}' );
			if ( suffixPos === -1 ) {
				suffixPos = oldContent.length;
			}
		} else {
			suffixPos = endMatch[ 1 ].length;
		}
		return oldContent.slice( 0, markerEndPos ) + '\n' +
			rows + '\n' + oldContent.slice( suffixPos );
	}

	/**
	 * Get the index page for a given focus area
	 *
	 * @param {string} area
	 * @param {string} lang
	 * @return {string}
	 */
	getWishIndexTitle( area, lang ) {
		let template;
		if ( area === 'all' || area === '' ) {
			template = config.wishIndexTemplateAll;
		} else if ( area === 'recent' ) {
			template = config.wishIndexTemplateRecent;
		} else if ( area === 'archive' ) {
			template = config.wishIndexTemplateArchive;
		} else {
			template = config.wishIndexTemplate + '/' + area;
		}
		if ( lang === '' || lang === 'en' ) {
			return `Template:${ template }`;
		} else {
			return `Template:${ template }/${ lang }`;
		}
	}

	/**
	 * Get a row for the wishlist index page.
	 *
	 * @param {Wish} wish
	 * @param {?string} basePage The page to use for the link, or null to use
	 *   the one from the wish. This is used to override the link to the base
	 *   language so that it goes to the base page.
	 * @param {string} baseUpdated The update date of the base page as an
	 *   ISO 8601 string
	 * @return {string}
	 */
	getWishIndexRow( wish, basePage, baseUpdated ) {
		return `{{${ config.wishIndexTemplate }/Row` +
			`|page=${ basePage || wish.page }` +
			`|title=${ wish.title }` +
			`|baselang=${ wish.baselang }` +
			`|lang=${ wish.lang }` +
			`|area=${ wish.area }` +
			`|type=${ wish.type }` +
			`|projects=${ wish.projects }` +
			`|tasks=${ wish.tasks }` +
			`|updated=${ baseUpdated }` +
			`|status=${ wish.status }}}`;
	}

}
