import BotCommand from './BotCommand.js';
import config from '../common/config.js';

export default class UpdateFocusAreasCommand extends BotCommand {

	setup( program ) {
		super.setup( program );
		program.description( 'Update the focus area list templates' );
	}

	async doRun() {
		// @todo Support more than 500 focus areas.
		const prefixedPages = await this.bot.getPagesByPrefix( config.focusAreaPagePrefix );
		const focusAreas = new Map();
		const votesPages = [];
		for ( const page of prefixedPages ) {
			const slug = page.split( '/' )[ 2 ];
			if ( slug.length < 6 ) {
				// Hacky way to avoid treating language-code subpages as focus areas.
				continue;
			}
			// Store initial vote count of 0.
			focusAreas.set( slug, 0 );
			// If this is a vote count subpage, store for later retrieval.
			if ( page.endsWith( config.focusAreaVoteCountSuffix ) ) {
				votesPages.push( page );
			}
		}
		// Get all vote counts' pages.
		for await ( const page of this.bot.readGen( votesPages ) ) {
			const slug = page.title.split( '/' )[ 2 ];
			focusAreas.set( slug, parseInt( page.revisions[ 0 ].content ) || 0 );
		}
		this.log( '[I] Found ' + focusAreas.size + ' focus areas' );
		const sorted = [ ...focusAreas ].sort( ( a, b ) => {
			return b[ 1 ] - a[ 1 ];
		} );
		this.editFocusAreaList( config.focusAreaTemplateTop, sorted.slice( 0, 4 ) );
		this.editFocusAreaList( config.focusAreaTemplateAll, sorted );
	}

	/**
	 * Update a single focus area list template with new slugs.
	 *
	 * @param {string} templateName
	 * @param {Array<string>} slugList
	 */
	editFocusAreaList( templateName, slugList ) {
		const title = 'Template:' + templateName;
		const list = slugList.map( ( e ) => e[ 0 ] ).join( '\n' );
		const wikitext = '{{' + config.focusAreasTemplate + '|\n' + list + '\n}}';
		const summary = 'Updating focus area list.';
		this.log( '[I] Updating focus area list: ' + templateName );
		this.bot.save( title, wikitext, summary );
	}

}
