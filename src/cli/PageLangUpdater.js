import { Mwn } from 'mwn';

export default class PageLangUpdater {
	/**
	 * @callback Logger
	 * @param {string} message
	 */

	/**
	 * @param {Mwn} bot
	 * @param {Logger} log
	 */
	constructor( bot, log ) {
		this.bot = bot;
		this.log = log;
		this.updates = {};
		this.validLangs = null;
	}

	/**
	 * Notify this object about a wish page that may need its page language
	 * updated.
	 *
	 * @param {Wish} wish
	 * @param {string} pageLang
	 */
	notify( wish, pageLang ) {
		if ( wish.lang !== '' ) {
			// Don't set the language on translated subpages
			return;
		}
		const targetLang = wish.baselang === '' ? 'en' : wish.baselang;
		if ( pageLang === targetLang ) {
			return;
		}
		this.updates[ wish.page ] = targetLang;
	}

	/**
	 * Perform the updates and flush the update queue
	 */
	async update() {
		if ( this.updates.length === 0 ) {
			this.log( '[I] No page languages updated' );
			return;
		}
		// Flush the queue
		const updates = this.updates;
		this.updates = {};

		// Get a list of languages for validation
		// We can cache it, to avoid continuously repeating an attempt to set
		// the page language to an invalid code
		const validCodes = await this.getValidLangs();

		for ( const title in updates ) {
			const lang = updates[ title ];
			if ( !validCodes.includes( lang ) ) {
				this.log( `[W] Page [[${ title }]] specifies invalid language ${ lang }` );
				continue;
			}

			await this.bot.request( {
				action: 'setpagelanguage',
				title: title,
				lang: lang,
				reason: 'Updating to reflect the baselang parameter in the template',
				token: this.bot.csrfToken
			} );
			this.log( `[S] Updated the language of [[${ title }]] to ${ lang }` );
		}
	}

	/**
	 * Get the list of valid language codes
	 *
	 * @return {Promise<string[]>}
	 */
	async getValidLangs() {
		if ( !this.validLangs ) {
			const res = await this.bot.query( {
				meta: 'siteinfo',
				siprop: 'languages'
			} );
			this.validLangs = [];
			for ( const lang of res.query.languages ) {
				this.validLangs.push( lang.code );
			}
		}
		return this.validLangs;
	}
}
