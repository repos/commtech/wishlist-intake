import Database from './Database.js';
import MetaDataStore from './MetaDataStore.js';
import { Mwn } from 'mwn';
import Change from './Change.js';

/**
 * An abstraction of a wiki change feed
 */
export default class RcFeed {
	/**
	 * @callback Logger
	 * @param {string} message
	 */

	/**
	 * @param {Database} db
	 * @param {MetaDataStore} metaDataStore
	 * @param {Mwn} bot
	 * @param {Logger} log
	 */
	constructor( db, metaDataStore, bot, log ) {
		this.db = db;
		this.metaDataStore = metaDataStore;
		this.bot = bot;
		this.log = log;
	}

	/**
	 * @callback FilterCallback
	 * @param {Change} change
	 * @return boolean
	 */

	/**
	 * @callback HandleCallback
	 * @param {Change} change
	 * @return {Promise<void>}
	 */

	/**
	 * Process any recent changes on the wiki that have not yet been processed.
	 *
	 * If the `filter` callback returns false, the change will be skipped. The
	 * `handle` function will not be called.
	 *
	 * If the `filter` callback returns true, the async `handle` callback will
	 * be called with the same change. If the `handle` callback completes
	 * without throwing an exception, we will note completion in the database
	 * and the callback will not be called for that change again.
	 *
	 * @param {?FilterCallback} filter
	 * @param {HandleCallback} handle
	 * @return {Promise<void>}
	 */
	async processChanges( filter, handle ) {
		const meta = await this.metaDataStore.getAll();
		const startTime = meta.rcTime;
		if ( !startTime ) {
			throw new Error( "There's no time in the metadata store, can't run RC feed" );
		}
		const startId = meta.rcId;
		this.log( `[I] Processing changes starting from ${ startTime.toISOString() } rc_id ${ startId }` );

		let lastTimestamp = null, lastId = null;

		for await ( const response of this.bot.continuedQueryGen( {
			action: 'query',
			list: 'recentchanges',
			rcprop: 'title|timestamp|ids|loginfo',
			rcstart: startTime,
			rcdir: 'newer',
			rclimit: 'max'
		} ) ) {
			for ( const data of response.query.recentchanges ) {
				const change = new Change( data );
				lastTimestamp = change.timestamp;
				lastId = change.rcId;
				if ( change.timestamp.getTime() === startTime.getTime() &&
					startId && change.rcId <= startId
				) {
					continue;
				}
				if ( filter && !filter( change ) ) {
					continue;
				}
				await this.db.transact( async () => {
					await handle( change );

					// Update meta during the same transaction, so that the
					// script will be safely abortable.
					await this.metaDataStore.set( {
						rcTime: change.timestamp,
						rcId: change.rcId
					} );
				} );

				// Already wrote these to metaDataStore
				lastTimestamp = lastId = null;
			}
		}

		// For efficiency, we don't commit rcTime to the database on each
		// filtered change. There might be a lot of them and it's not necessary
		// for DB consistency. But we still need to update it at the end, so
		// that a smaller number of changes will be processed next time the
		// script is run.
		if ( lastTimestamp !== null && lastId !== null && lastId !== startId ) {
			await this.metaDataStore.set( {
				rcTime: lastTimestamp,
				rcId: lastId
			} );
		}
	}

	/**
	 * Set the start time for RC feed processing. In a future call to
	 * processChanges(), only events equal to or after this time will be
	 * requested.
	 *
	 * processChanges() updates the start time automatically after successful
	 * execution, but it has to be set externally before the first call to
	 * processChanges().
	 *
	 * @param {Date} date
	 * @return {Promise<void>}
	 */
	async setStartTime( date ) {
		await this.metaDataStore.set( { rcTime: date } );
	}
}
