CREATE TABLE wish (
	wish_page_id INT UNSIGNED NOT NULL,
	wish_page VARCHAR(300) NOT NULL,
	wish_lang VARCHAR(100) NOT NULL,
	wish_base_lang VARCHAR(100) NOT NULL,
	wish_name VARCHAR(300) NOT NULL,
	wish_type MEDIUMTEXT NOT NULL,
	wish_status MEDIUMTEXT NOT NULL,
	wish_title MEDIUMTEXT NOT NULL,
	wish_description MEDIUMTEXT NOT NULL,
	wish_audience MEDIUMTEXT NOT NULL,
	wish_tasks MEDIUMTEXT NOT NULL,
	wish_proposer MEDIUMTEXT NOT NULL,
	wish_created MEDIUMTEXT NOT NULL,
	wish_created_dt DATETIME NOT NULL,
	wish_updated_dt DATETIME NOT NULL,
	wish_projects MEDIUMTEXT NOT NULL,
	wish_other_project MEDIUMTEXT NOT NULL,
	wish_area MEDIUMTEXT NOT NULL,
	PRIMARY KEY (wish_page_id),
	INDEX (wish_created_dt)
) CHARACTER SET utf8mb4;

CREATE TABLE meta (
	meta_name VARCHAR(255) NOT NULL,
	meta_value MEDIUMTEXT,
	PRIMARY KEY (meta_name)
) CHARACTER SET utf8mb4;
