import BotCommand from './BotCommand.js';
import fs from 'fs';

export default class PullCommand extends BotCommand {
	setup( program ) {
		super.setup( program );
		program
			.description( 'Pull the content of a page from the wiki' )
			.option( '--page <page-title>', 'The title of the page to pull from' );
	}

	async doRun() {
		if ( this.options.page ) {
			await this.pullPage( this.options.page );
		} else {
			await this.pullAll();
		}
	}

	/**
	 * Pull the content of a single page from the wiki
	 *
	 * @param {string} pageTitle
	 */
	async pullPage( pageTitle ) {
		if ( !pageTitle ) {
			throw new Error( 'You must provide a page title, or use the --all option' );
		}
		process.stdout.write( `Pulling [[${ pageTitle }]] to local repository… ` );
		const pageObj = await this.bot.read( pageTitle );
		if ( pageObj.missing ) {
			process.stdout.write( '\x1b[33mmissing from the wiki, skipping.\x1b[0m\n' );
			return;
		}
		const content = ( await this.bot.read( pageTitle ) ).revisions[ 0 ].content;
		fs.writeFileSync( await this.getFilePathFromPageTitle( pageTitle ), content + '\n' );
		process.stdout.write( 'done\n' );
	}

	/**
	 * Pull all pages from the wiki that are known to exist locally in the repo.
	 */
	async pullAll() {
		const dir = './src/mediawiki-pages/';
		for await ( const file of this.getFiles( dir ) ) {
			// Always skip demo files, etc.
			if ( file.includes( '/demo-only/' ) || file.includes( '/abuse-filters/' ) ||
				file.includes( '/dependencies/' )
			) {
				continue;
			}

			const pageTitle = this.normalizePageTitle( file.split( '/' ).pop() );
			if ( pageTitle === 'README' ) {
				continue;
			}

			await this.pullPage( pageTitle );
		}
	}
}
