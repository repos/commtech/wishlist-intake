import Database from './Database.js';

const FIELD_DECLARATIONS = {
	// A global version number which can be used to invalidate the cache when
	// the software changes
	wishVersion: 'number',
	// The latest revision timestamp of wishes included in the wish table
	wishCacheTime: 'date',
	// The latest revision timestamp of wishes included in the wiki index pages
	wishIndexTime: 'date',
	// The latest RC timestamp processed (not including ignored events)
	rcTime: 'date',
	// The latest rc_id processed (not including ignored events)
	rcId: 'number'
};

/**
 * A key/value store in the local database
 */
export default class MetaDataStore {
	/**
	 * @param {Database} db
	 */
	constructor( db ) {
		this.db = db;
		this.existenceCache = {};
	}

	/**
	 * Get all metadata
	 *
	 * @return {Promise<Object>}
	 */
	async getAll() {
		const res = await this.db.query( 'SELECT meta_name, meta_value FROM meta' );
		const meta = {};
		for ( const row of res ) {
			const name = row.meta_name;
			let value = row.meta_value;
			switch ( FIELD_DECLARATIONS[ name ] ) {
				case 'date':
					value = new Date( value );
					break;
				case 'number':
					value = parseFloat( value );
					break;
			}
			meta[ name ] = value;
			this.existenceCache[ name ] = true;
		}
		return meta;
	}

	/**
	 * Set metadata
	 *
	 * @param {Object} newMeta Mapping of new values to store
	 * @return {Promise<void>}
	 */
	async set( newMeta ) {
		let needInsert = false;
		for ( const name in newMeta ) {
			if ( !FIELD_DECLARATIONS[ name ] ) {
				throw new Error( `Unknown meta field "${ name }"` );
			}
			if ( !this.existenceCache[ name ] ) {
				needInsert = true;
				break;
			}
		}

		if ( needInsert ) {
			const oldMeta = await this.getAll();
			for ( const name in newMeta ) {
				const value = this.serialize( newMeta[ name ] );
				this.existenceCache[ name ] = true;
				if ( oldMeta[ name ] ) {
					await this.db.query(
						'UPDATE meta SET meta_value=? WHERE meta_name=?',
						[ value, name ]
					);
				} else {
					await this.db.query(
						'INSERT INTO meta (meta_name, meta_value) VALUES (?, ?)',
						[ name, value ]
					);
				}
			}
		} else {
			for ( const name in newMeta ) {
				const value = this.serialize( newMeta[ name ] );
				await this.db.query(
					'UPDATE meta SET meta_value=? WHERE meta_name=?',
					[ value, name ]
				);
			}
		}
	}

	/**
	 * Convert a value to a string
	 *
	 * @param {any} value
	 * @return {string}
	 */
	serialize( value ) {
		if ( value instanceof Date ) {
			return value.toISOString();
		} else {
			return String( value );
		}
	}
}
