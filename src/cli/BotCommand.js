import axios from 'axios';
import fs from 'fs';
import { resolve } from 'path';
import { execSync } from 'child_process';
import { Mwn } from 'mwn';
import BaseCommand from './BaseCommand.js';
import config from '../common/config.js';

export default class BotCommand extends BaseCommand {
	constructor() {
		super();
		/** @type {?Mwn} */
		this.bot = null;
		this.log = Mwn.log;
	}

	/**
	 * Execute the command
	 */
	async run() {
		this.program.parse();
		await this.login();
		await this.doRun();
		await this.shutdown();
	}

	setup( program ) {
		super.setup( program );
		program
			.option( '-c, --credentials <credentials>',
				'The credentials to use for logging in when multiple credentials exist.' )
			.option( '--debug-requests',
				'Dump HTTP request data' );
	}

	/**
	 * Get the credentials from `credentials.json`.
	 *
	 * @return {Object}
	 */
	get credentials() {
		if ( !this._credentials ) {
			let credentialsOption = this.options.credentials;
			let credentials = JSON.parse( fs.readFileSync(
				this.baseDir + '/config/credentials.json', 'utf-8' ) );
			const firstKey = Object.keys( credentials )[ 0 ];
			const firstEntry = credentials[ firstKey ];
			if ( typeof firstEntry === 'object' && !credentialsOption ) {
				console.warn(
					'Multiple credentials detected and no --credentials argument specified, ' +
					`defaulting to "${ firstKey }".`
				);
				credentialsOption = firstKey;
			}
			if ( credentialsOption ) {
				credentials = credentials[ credentialsOption ];
				if ( !credentials ) {
					console.error( `\x1b[31mCredentials for "${ credentialsOption }" not found.\x1b[0m` );
					// eslint-disable-next-line n/no-process-exit
					process.exit( 1 );
				}
			}
			this._credentials = credentials;
		}
		return this._credentials;
	}

	/**
	 * Recursively read a directory. To be replaced with `recursive: true` option
	 * when we're on Node 20+
	 *
	 * @param {string} dir The directory to read.
	 * @yield {string[]} The files in the directory.
	 * @see https://stackoverflow.com/a/45130990/604142 (CC BY-SA 4.0)
	 */
	async *getFiles( dir ) {
		const dirents = await fs.promises.readdir( dir, { withFileTypes: true } );
		for ( const dirent of dirents ) {
			const res = resolve( dir, dirent.name );
			if ( dirent.isDirectory() ) {
				yield* this.getFiles( res );
			} else {
				yield res;
			}
		}
	}

	/**
	 * Normalize the filename for a page into the MediaWiki page title.
	 *
	 * @param {string} pageTitle
	 * @return {string}
	 */
	normalizePageTitle( pageTitle ) {
		// We use .mediawiki and .lua extensions for IDEs, but they shouldn't be there on the wiki.
		pageTitle = pageTitle.replace( /\.(mediawiki|lua)$/, '' );
		// No forward-slashes in Linux filenames, so we use `~` to indicate subpages.
		return pageTitle.replaceAll( '~', '/' );
	}

	/**
	 * Normalize a MediaWiki page title into a filename.
	 * Inverse of `normalizePageTitle`.
	 *
	 * @param {string} pageTitle
	 * @return {string}
	 */
	normalizeFileName( pageTitle ) {
		let fileExtension = '.mediawiki';
		if ( /(js|css)$/.test( pageTitle ) ) {
			fileExtension = '';
		} else if ( /^Module:/.test( pageTitle ) ) {
			fileExtension = '.lua';
		}
		return pageTitle.replaceAll( ' ', '_' )
			.replaceAll( '/', '~' ) + fileExtension;
	}

	/**
	 * Get the full file path for a MediaWiki page title.
	 * If the file does not exist, it is assumed to be under `src/mediawiki-pages`.
	 *
	 * @param {string} pageTitle
	 * @return {Promise<string>}
	 */
	async getFilePathFromPageTitle( pageTitle ) {
		const dir = './src/mediawiki-pages/';
		for await ( const file of this.getFiles( dir ) ) {
			if ( this.normalizeFileName( pageTitle ) === file.split( '/' ).pop() ) {
				return file;
			}
		}
		return dir + this.normalizeFileName( pageTitle );
	}

	/**
	 * Generate an edit summary from the latest git commit.
	 *
	 * @param {string} [summary] Summary to use instead of the latest commit message.
	 * @return {string} In format `v1.2.3 at 1234567: Commit message`
	 */
	getAutoEditSummary( summary ) {
		const sha = execSync( 'git rev-parse --short HEAD' ).toString();
		summary = summary || execSync( 'git log -1 --pretty=%B' ).toString().trim();
		return `v${ this.version } at ${ sha.trim() }: ${ summary }`;
	}

	/**
	 * Get a logged-in instance of Mwn
	 *
	 * @return {Promise<Mwn>}
	 */
	async login() {
		if ( !this.bot ) {
			this.setupInterceptors();
			await ( async () => {
				const credentials = this.credentials;
				// Login to the wiki.
				this.bot = await Mwn.init( {
					apiUrl: credentials.apiUrl,
					username: credentials.username,
					password: credentials.password,
					userAgent: `WishlistIntakeBot/${ this.version } ([[User:Community Tech bot]])`,
					defaultParams: {
						assert: credentials.username.startsWith( 'Community Tech bot' ) ? 'bot' : 'user'
					}
				} );
			} )();
		}
		return this.bot;
	}

	/**
	 * Set up interceptors if requested
	 */
	setupInterceptors() {
		if ( this.options.debugRequests ) {
			axios.interceptors.request.use( ( reqConfig ) => {
				this.log( `[D] ${ reqConfig.method.toUpperCase() } ${ reqConfig.url }` );
				if ( reqConfig.data ) {
					if ( typeof reqConfig.data === 'string' ) {
						this.log( `[D]   ${ reqConfig.data }` );
					} else if ( reqConfig instanceof Object && reqConfig.prototype === Object ) {
						for ( const name in reqConfig.data ) {
							this.log( `[D]   ${ name }=${ reqConfig.data[ name ] }` );
						}
					} else {
						this.log( `[D]   ${ String( reqConfig.data ) }` );
					}
				} else if ( reqConfig.params ) {
					for ( const name in reqConfig.params ) {
						this.log( `[D]   ${ name }=${ reqConfig.params[ name ] }` );
					}
				}
				return reqConfig;
			} );
		}
	}

	getLogger() {
		return Mwn.log;
	}

	/**
	 * Add a page to the specified aggregate message group.
	 *
	 * @param {Mwn} bot
	 * @param {string} pageTitle
	 * @param {string} groupId
	 * @return {Promise}
	 */
	static addPageToAggregateGroup( bot, pageTitle, groupId = config.wishesMessageGroupId ) {
		return bot.request( {
			action: 'aggregategroups',
			do: 'associate',
			aggregategroup: groupId,
			group: `page-${ pageTitle.replaceAll( '_', ' ' ) }`,
			token: bot.csrfToken
		} );
	}

	/**
	 * Add a page to the default aggregate message group.
	 *
	 * @param {string} pageTitle
	 * @param {string} groupId
	 * @return {Promise}
	 */
	addPageToAggregateGroup( pageTitle, groupId = config.wishesMessageGroupId ) {
		return BotCommand.addPageToAggregateGroup( this.bot, pageTitle, groupId );
	}
}
