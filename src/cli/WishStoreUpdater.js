import config from '../common/config.js';
import Change from './Change.js';
import Database from './Database.js';
import { Mwn } from 'mwn';
import Util from '../common/Util.js';
import WishStore from './WishStore.js';
import RcFeed from './RcFeed.js';
import PageLangUpdater from './PageLangUpdater.js';
import BotCommand from './BotCommand.js';

/**
 * A class which updates the wish store based on data retrieved from the wiki
 */
export default class WishStoreUpdater {
	/**
	 * @callback Logger
	 * @param {string} message
	 */

	/**
	 * @param {Database} db
	 * @param {WishStore} wishStore
	 * @param {RcFeed} rcFeed
	 * @param {PageLangUpdater} pageLangUpdater
	 * @param {Mwn} bot
	 * @param {Logger} log
	 */
	constructor( db, wishStore, rcFeed, pageLangUpdater, bot, log ) {
		this.db = db;
		this.wishStore = wishStore;
		this.rcFeed = rcFeed;
		this.pageLangUpdater = pageLangUpdater;
		this.bot = bot;
		this.log = log;
	}

	/**
	 * Truncate and rebuild the wish cache
	 *
	 * @return {Promise<void>}
	 */
	async rebuild() {
		const wishlistTemplate = Util.getWishTemplate();
		this.log( '[I] Starting rebuild' );
		await this.db.transact( async () => {
			const startTime = new Date();
			await this.wishStore.truncate();
			for await ( const response of this.bot.continuedQueryGen( {
				action: 'query',
				prop: 'revisions|info',
				generator: 'allpages',
				rvprop: 'ids|timestamp|content',
				rvslots: 'main',
				gapprefix: config.wishPagePrefix,
				gaplimit: '100'
			} ) ) {
				if ( !response?.query?.pages ) {
					// Empty result
					continue;
				}
				for ( const page of response.query.pages ) {
					const wish = wishlistTemplate.getWish(
						wishlistTemplate.stripTranslate( page.revisions[ 0 ].slots.main.content ),
						page.title,
						page.pageid,
						page.revisions[ 0 ].timestamp
					);
					if ( !wish ) {
						this.log( `[W] No wish found on page [[${ page.title }]]` );
						continue;
					}
					this.log( `[I] Adding wish page [[${ page.title }]]` );
					await this.wishStore.insertWish( wish );
					this.pageLangUpdater.notify( wish, page.pagelanguage );
				}
			}
			await this.wishStore.setCacheTime( startTime );
		} );
		this.log( '[S] Rebuild complete' );
	}

	/**
	 * Update the wish cache based on events from recentchanges.
	 *
	 * @return {Promise<void>}
	 */
	async update() {
		const wishlistTemplate = Util.getWishTemplate();

		/**
		 * @param {Change} change
		 * @return {boolean}
		 */
		function filter( change ) {
			return change.title && change.title.startsWith( config.wishPagePrefix );
		}

		/**
		 * When a wish is marked for translation, add the page to the aggregate group
		 * and make a null edit to fix categorization.
		 *
		 * @param {Change} change
		 */
		async function handleMarkedForTranslation( change ) {
			this.log( `[I] Page marked for translation: [[${ change.title }]]` );
			try {
				await BotCommand.addPageToAggregateGroup( this.bot, change.title );
			} catch ( e ) {
				if ( e instanceof Mwn.Error ) {
					this.log( `[E] Unable to add to aggregate group. ${ e.info }` );
				} else {
					throw e;
				}
			}
			await this.bot.purge( change.title, { forcelinkupdate: true } );
			await this.wishStore.setCacheTime( change.timestamp );
		}

		/**
		 * @param {Change} change
		 */
		async function handle( change ) {
			if ( change.isDelete() ) {
				this.log( `[I] Page deleted: [[${ change.title }]]` );
				await this.wishStore.deleteWishByPageId( change.pageId );
				await this.wishStore.setCacheTime( change.timestamp );
				return;
			}

			if ( change.isMarkForTranslation() ) {
				await handleMarkedForTranslation.call( this, change );
				// No changes are made with this log action.
				return;
			}

			let title = change.title;
			if ( change.isMove() ) {
				this.log( `[I] Page moved from [[${ change.title }]] to [[${ change.moveTarget }]]` );
				await this.wishStore.deleteWishByPageId( change.pageId );
				title = change.moveTarget;
			}

			const response = await this.bot.query( {
				prop: 'revisions|info',
				rvprop: 'ids|timestamp|content',
				rvslots: 'main',
				titles: title
			} );
			const page = response?.query?.pages?.[ 0 ];
			const revision = page?.revisions?.[ 0 ];
			const content = revision?.slots?.main?.content;
			if ( content === undefined ) {
				this.log( `[W] Failed to load content of updated page [[${ title }]]` );
				return;
			}
			const wish = wishlistTemplate.getWish(
				wishlistTemplate.stripTranslate( content ),
				title,
				page.pageid,
				revision.timestamp
			);
			if ( !wish ) {
				this.log( `[W] No wish found on page [[${ title }]]` );
				await this.wishStore.deleteWishByPageId( change.pageId );
				await this.wishStore.setCacheTime( change.timestamp );
				return;
			}
			await this.wishStore.upsertWish( wish );
			await this.wishStore.setCacheTime( change.timestamp );
			this.pageLangUpdater.notify( wish, page.pagelanguage );
			this.log( `[I] Processed updated page [[${ title }]]` );
		}

		this.log( '[I] Updating cache from recent changes' );
		await this.rcFeed.processChanges(
			filter,
			handle.bind( this )
		);
		this.log( '[S] Cache update complete' );
	}
}
