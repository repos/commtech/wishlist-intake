/**
 * Object representation of a recent change
 */
export default class Change {
	/**
	 * @param {Object} data API response data
	 */
	constructor( data ) {
		this.title = data.title;
		this.pageId = data.pageid;
		this.rcId = data.rcid;
		this.timestamp = new Date( data.timestamp );
		this.logType = data.logtype;
		this.logAction = data.logaction;
		this.logParams = data.logparams;
	}

	/**
	 * @return {boolean}
	 */
	isDelete() {
		return this.logType === 'delete' &&
			[ 'delete', 'delete_redir', 'delete_redir2' ].includes( this.logAction );
	}

	/**
	 * @return {boolean}
	 */
	isMove() {
		return this.logType === 'move' &&
			[ 'move', 'move_redir' ].includes( this.logAction );
	}

	/**
	 * @return {boolean}
	 */
	isMarkForTranslation() {
		return this.logType === 'pagetranslation' &&
			this.logAction === 'mark';
	}

	/**
	 * @return {string}
	 */
	get moveTarget() {
		return this.isMove() ? this.logParams.target_title : null;
	}
}
