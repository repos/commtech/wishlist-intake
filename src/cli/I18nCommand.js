import BaseCommand from './BaseCommand.js';
import config from '../common/config.js';
import fs from 'fs';

export default class I18nCommand extends BaseCommand {

	setup( program ) {
		program.description( 'Generate a translatable wikitext-based JSON file for use by the gadget.' );
	}

	async doRun() {
		let pageTitle = config.messagesPage;
		const messages = config.messages;
		process.stdout.write( `Generating source file for ${ pageTitle } from config.messages… ` );

		// Generate the JSON with <translate> tags.
		for ( const [ key, value ] of Object.entries( messages ) ) {
			messages[ key ] = `<translate nowrap><!--T:${ key }--> ${ value }</translate>`;
		}
		const out = {
			'@languages': '<languages/>',
			'@categories': '[[Category:Community Wishlist/Messages{{#translation:}}]]',
			messages
		};

		// Normalize page title for filesystem
		pageTitle = pageTitle.replaceAll( ' ', '_' )
			.replaceAll( '/', '~' );

		// Write the JSON to the filesystem, later to be deployed by the deployment script.
		await fs.promises.writeFile(
			`${ this.baseDir }/src/mediawiki-pages/${ pageTitle }.mediawiki`,
			JSON.stringify( out, null, 4 ),
			{ encoding: 'utf-8' }
		);

		process.stdout.write( 'done.\n' );
	}
}
