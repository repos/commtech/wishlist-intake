import BotCommand from './BotCommand.js';

export default class UpdateWishTalksCommand extends BotCommand {
	setup( program ) {
		super.setup( program );
		program
			.description( 'Loops through wish talk pages and adds templates if missing' )
			.option( '--dry', 'Do not make any changes' )
			.option( '--limit <n>', 'Limit the number of pages to process' );
	}

	async doRun() {
		await this.addMissingTemplates();
	}

	/**
	 * Check wish talk pages for missing templates and add them
	 *
	 * @return {Promise<void>}
	 */
	async addMissingTemplates() {
		this.log( '[I] Checking for missing talk page templates' );

		let wishtalkpages = [];
		// Make API search request:
		//   Find all prefix'd pages in the talk namespace that don't have the template
		//   Iterate over the results if needed
		for await ( const json of this.bot.continuedQueryGen( {
			action: 'query',
			list: 'search',
			srlimit: this.options.limit || 'max',
			format: 'json',
			utf8: 1,
			formatversion: 2,
			srsearch: '-insource:/\\{\\{Community Wishlist\\/Talk/ prefix:Talk:Community_Wishlist/Wishes/',
			srnamespace: 1
		} ) ) {
			wishtalkpages = wishtalkpages.concat( json.query.search.map( ( page ) => page.title ) );
		}

		let processed = 0;

		for ( const title of wishtalkpages ) {
			await this.bot.edit( title, ( rev ) => {
				// Check, as results could be lagged
				if ( !rev.content.includes( '{{Community Wishlist/Talk}}' ) ) {
					rev.content = '{{Community Wishlist/Talk}}\n' + rev.content;

					if ( this.options.dry ) {
						this.log( `[W] Dry run, skipping adding template to ${ title }` );
						return;
					} else {
						this.log( `[I] Adding template to ${ title }` );
					}
				} else {
					this.log( `[S] ${ title } already has the template` );
					return;
				}
				return {
					text: rev.content,
					summary: 'Adding [[Template:Community Wishlist/Talk|Community Wishlist/Talk]] template',
					minor: true,
					bot: true
				};
			} );
			processed++;
		}
		this.log( `[I] Processed ${ processed } pages` );
	}
}
