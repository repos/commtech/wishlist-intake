import BaseCommand from './BaseCommand.js';
import fs from 'fs';

export default class InstallDbCommand extends BaseCommand {
	setup( program ) {
		super.setup( program );
		program
			.description( 'Install or reinstall local DB tables for bot-related data' )
			.option( '--reinstall', 'Drop and recreate the tables if they exist.' );
	}

	async doRun() {
		const options = this.options;
		const config = this.config;

		process.stdout.write( `Connecting to host ${ config.mysqlHost } as user ${ config.mysqlUser }\n` );
		const db = await this.getDB();
		if ( options.reinstall ) {
			await db.query( 'DROP TABLE IF EXISTS wish' );
			await db.query( 'DROP TABLE IF EXISTS meta' );
		}
		await db.query( fs.readFileSync( this.baseDir + '/src/cli/sql/tables.sql', 'utf-8' ) );
		process.stdout.write( 'Done\n' );
	}
}
