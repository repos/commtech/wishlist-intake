import { Command } from 'commander';
import fs from 'fs';
import path from 'path';
import commonConfig from '../common/config.js';
import mysql from 'mysql2/promise';
import Database from './Database.js';

export default class BaseCommand {

	constructor() {
		this.program = new Command();
		this.setup( this.program );
	}

	/**
	 * Execute the command
	 *
	 * @return {Promise<void>}
	 */
	async run() {
		this.program.parse();
		await this.doRun();
		await this.shutdown();
	}

	/**
	 * Override this to set up the program options
	 *
	 * @param {Command} program
	 */
	setup( program ) {
		program
			.option( '--debug-queries',
				'Write a debug message for each SQL query' );
	}

	/**
	 * Override this to execute the command
	 *
	 * @return {Promise<void>}
	 */
	async doRun() {
	}

	/**
	 * Get the parsed program options
	 *
	 * @return {Object}
	 */
	get options() {
		return this.program.opts();
	}

	/**
	 * Get the version of the bot from `package.json`.
	 * @return {string}
	 */
	get version() {
		if ( !this._version ) {
			this._version = JSON.parse( fs.readFileSync( this.baseDir + '/package.json', 'utf-8' ) ).version;
		}
		return this._version;
	}

	/**
	 * Get the base directory of the wishlist-intake installation
	 * @return {string}
	 */
	get baseDir() {
		if ( !this._baseDir ) {
			let dir;
			/* eslint-disable es-x/no-import-meta */
			if ( import.meta.dirname ) {
				// Node 20
				dir = import.meta.dirname;
			} else {
				// Node 18
				dir = path.dirname(
					decodeURIComponent( new URL( import.meta.url ).pathname ) );
			}
			/* eslint-enable */
			this._baseDir = path.dirname( path.dirname( dir ) );
		}
		return this._baseDir;
	}

	/**
	 * Get cli-config.json merged with config.json
	 *
	 * @return {Object}
	 */
	get config() {
		if ( !this._config ) {
			const configPath = this.baseDir + '/config/cli-config.json';
			if ( !fs.existsSync( configPath ) ) {
				console.error( 'cli-config.json not found. ' +
					'Copy it from cli-config.json.dist and edit it to provide DB connection parameters.' );
				// eslint-disable-next-line n/no-process-exit
				process.exit( 1 );
			}
			this._config = JSON.parse( fs.readFileSync( configPath, 'utf-8' ) );

			// Merge with config.json
			Object.assign( this._config, commonConfig );
		}
		return this._config;
	}

	/**
	 * Get a connection to the local cache database
	 *
	 * @return {Database}
	 */
	async getDB() {
		if ( !this._db ) {
			const config = this.config;
			this._db = new Database(
				await mysql.createConnection( {
					host: config.mysqlHost,
					user: config.mysqlUser,
					password: config.mysqlPassword,
					database: config.mysqlDatabase,
					multipleStatements: true
				} ),
				{
					debugQueries: this.options.debugQueries
				},
				this.getLogger()
			);
		}
		return this._db;
	}

	/**
	 * Close the DB connection and any similar resources. The event loop will
	 * not terminate until this is called.
	 *
	 * @return {Promise<void>}
	 */
	async shutdown() {
		if ( this._db ) {
			await this._db.end();
		}
		console.log( `\x1b[32mCompleted after ${ parseFloat( process.uptime().toString() ).toFixed( 3 ) } seconds.\x1b[0m\n` );
	}

	/**
	 * @callback Logger
	 * @param {string} message
	 */

	/**
	 * Get a function for debug logging. BotCommand overrides this.
	 *
	 * @return {Logger}
	 */
	getLogger() {
		return ( message ) => {
			console.log( message );
		};
	}
}
