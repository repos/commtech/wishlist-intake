<noinclude><languages/></noinclude>{{#switch:{{lc:{{{1|<noinclude>open</noinclude>}}}}}
|feature=<translate><!--T:1--> Feature request</translate>
|bug=<translate><!--T:2--> Bug fix</translate>
|change=<translate><!--T:3--> System change</translate>
|#default=<translate><!--T:4--> Unknown</translate>
}}<noinclude>{{documentation}}</noinclude>
