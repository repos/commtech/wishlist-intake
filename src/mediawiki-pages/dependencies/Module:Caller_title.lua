-- From https://meta.wikimedia.org/wiki/Module:Caller_title
local p = {}

function p.title(frame)
	return frame:getParent():getTitle()
end

function p.lang(frame)
	local base = frame.args.base
	local title = p.title(frame)
	if base ~= title then
		local parts = mw.text.split(p.title(frame), '/', true)
		return parts[#parts]
	else
		-- we’re on the base page of the translation (directly, it’s not translated from somewhere),
		-- so we have no subpage language code, but we use PAGELANGUAGE
		return frame:preprocess('{{PAGELANGUAGE}}')
	end
end

return p
