-- Based on [[Module:Clickable button 2]]

local p = {}

function p.main(frame)
	local args = require('Module:Arguments').getArgs(frame, {
		wrappers = 'Template:Cdx-button'
	})
	return p.luaMain(args)
end

function p.luaMain(args)
	if not args.link and not args.url then
		return ''
	end
	local data = p.makeLinkData(args)
	local link = p.renderLink(data)
	return link
end

function p.makeLinkData(args)
	local data = {}

	-- Get the link and label values, and find whether we are outputting a
	-- wikilink or a URL.
	if args.url then
		data.isUrl = true
		data.url = args.url
		if args.label then
			data.label = args.label
		else
			data.label = args.url
		end
	else
		data.isUrl = false
		data.link = args.link
		if args.label then
			data.label = args.label
		end
	end

	-- Classes
	local class = args.class and args.class:lower()
	data.classes = {
		'cdx-button cdx-button--fake-button'
	}
	local action = args.action or 'default'
	local weight  = args.weight or 'normal'
	local endisabled = 'enabled'
	if args.disabled then
		endisabled = 'disabled'
	end
	table.insert(data.classes, 'cdx-button--action-' .. action)
	table.insert(data.classes, 'cdx-button--weight-' .. weight)
	table.insert(data.classes, 'cdx-button--fake-button--' .. endisabled)
	if class then
		table.insert(data.classes, class)
	end

	-- Icon
	local icon = args.icon and args.icon:lower()
	if icon then
		data.iconSpan = mw.html.create('span')
		data.iconSpan:addClass('cdx-button__icon cdx-demo-css-icon--' .. icon)
		data.iconSpan:attr('aria-hidden', 'true')
	end

	return data
end

function p.renderLink(data)
	-- Render the span tag.
	local display
	do
		local displaySpan = mw.html.create('span')
		for i, class in ipairs(data.classes or {}) do
			displaySpan:addClass(class)
		end
		displaySpan
			:attr('role', 'button')
			:attr('aria-disabled', 'false')
		if data.iconSpan then
			displaySpan:node(data.iconSpan)
		end
		displaySpan:wikitext(data.label)
		display = tostring(displaySpan)
	end

	-- Render the link
	local link
	if data.isUrl then
		mw.logObject(data)
		link = string.format('[%s %s]', data.url, display)
	else
		link = string.format('[[%s|%s]]', data.link, display)
	end

	return string.format('<span class="plainlinks">%s</span>', link)
end

return p
