-- from https://meta.wikimedia.org/w/index.php?title=Module:Shuffle&oldid=26772785
local p = {}

function p._randomize( rows )
    math.randomseed( os.time() );
    local output = {};
    while #rows > 0 do
        local row = mw.text.trim( table.remove( rows, math.random( #rows ) ) );
        table.insert( output, row );
    end
    return output
end

function p.main( frame )
    local rows = {}
    for i, row in ipairs( frame:getParent().args ) do
        table.insert( rows, row );
    end

    return table.concat( p._randomize( rows ), '\n' )
end

return p;
