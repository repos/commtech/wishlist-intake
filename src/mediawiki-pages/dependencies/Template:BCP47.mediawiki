<!-- From https://meta.wikimedia.org/wiki/Template:BCP47 -->{{#switch:{{lc:{{{1|}}}}}
<!-- pseudo codes -->
||root|default|i-default=<!-- empty language tag means unknown per the HTML spec --><!-- or ={{int:lang}}? (the best user default), or =en? (the default "ContentLanguage" for unlocalized data on Meta) -->

<!-- current BCP47 violations by Wikimedia sites, which can be fixed using standard tags when they exist -->
|als=gsw
|bat-smg=sgs
|de-formal=de<!-- could be "de-x-formal", but actually a subset within standard "de" for HTML/XML -->
|eml=egl<!-- retired code, the de facto eml.wikipedia uses Emilian, not Romagnol -->
|fiu-vro=vro
|mo=ro-cyrl<!-- retired, best fit on Wikimedia sites, but no longer working in interwikis (Wikipedia project deleted) -->
|nl-informal=nl<!-- could be "nl-x-informal", but actually a subset within standard "nl" for HTML/XML -->
|nrm=nrf<!-- Wikimedia sites uses "nrm" to mean Norman, but standard "nrm" is an unrelated language. The "nrf" code is now standardized for Norman (previously used a private-use extension of French "fr-x-nrm") -->
|roa-rup=rup
|simple=en<!-- could be "en-simple" but actually a subset within standard "en" for HTML -->
|sr-ec=sr-cyrl
|sr-el=sr-latn
|zh-classical=lzh

<!-- other current BCP47 violations by Wikimedia sites, fixed using private-use extensions (if they are needed, labels are limited to 8 letters/digits) -->
|cbk-zam=cbk-x-zam
|map-bms=jv-x-bms
|roa-tara=it-x-tara
|tokipona|tp=x-tokipona

<!-- conforming BCP47 "private-use" extensions used by Wikimedia, which are no longer needed, and improved using now standard codes -->
|be-x-old=be-tarask

<!-- conforming but ambiguous BCP47 codes used by Wikimedia in a more restrictive way, with more precision -->
|arc=syc<!-- The de-facto arc.wikipedia.org, as per their community request, is in actual using Syriac which is coded as syc -->
|no=nb<!-- "no" means Bokmål on Wikimedia sites, "nb" is not used -->
|bh=bho<!-- "bh"="bih" is a language family, interpreted in Wikimedia as the single language "bho", even if its interwiki code remains bh) -->
|tgl=tl-tglg<!-- "tgl" on Wikimedia is the historic variant of the Tagalog macrolanguage ("tl" or "tgl", "tl" recommended for BCP47), written in the Baybayin script ("tglg") -->

<!-- conforming BCP47 "inherited" tags, strongly discouraged and replaced by their recommended tags (complete list that should not be augmented now) -->
|art-lojban=jbo<!-- still used in some old Wikimedia templates -->
|en-gb-oed=en-gb<!-- no preferred replacement, could be "en-gb-x-oed" but actually a subset within standard "en-gb" -->
|i-ami=ami
|i-bnn=bnn
|i-hak=hak
|i-klingon=tlh
|i-lux=lb
|i-navajo=nv
|i-pwn=pwn
|i-tao=tao
|i-tay=tay
|i-tsu=tstu
|no-bok=nb<!-- still used in some old Wikimedia templates -->
|no-nyn=nn<!-- still used in some old Wikimedia templates -->
|sgn-be-fr=sfb
|sgn-be-nl=vgt
|sgn-ch-de=sgg
|zh-guoyu=cmn<!-- this could be an alias of "zh" on Wikimedia sites, which do not use "cmn" but assume "zh" is Mandarin -->
|zh-hakka=hak
|zh-min=zh-tw<!-- no preferred replacement, could be "zh-x-min", but actually a subset within standard "zh-tw"; not necessarily "nan" -->
|zh-min-nan=nan<!-- used in some old Wikimedia templates and in interwikis -->
|zh-xiang=hsn

<!-- conforming BCP47 "redundant" tags, discouraged and replaced by their recommended tags (complete list that should not be augmented now) -->
|sgn-br=bzs
|sgn-co=csn
|sgn-de=gsg
|sgn-dk=dsl
|sgn-es=ssp
|sgn-fr=fsl<!-- still used in some old Wikimedia templates -->
|sgn-gb=bfi
|sgn-gr=gss
|sgn-ie=isg
|sgn-it=ise
|sgn-jp=jsl
|sgn-mx=mfs
|sgn-ni=ncs
|sgn-nl=dse
|sgn-no=nsl
|sgn-pt=psr
|sgn-se=swl
|sgn-us=ase<!-- still used in some old Wikimedia templates -->
|sgn-za=sfs
|zh-cmn=cmn<!-- still used in some old Wikimedia templates, this could be an alias of "zh" on Wikimedia sites, which do not use "cmn" but assume "zh" is Mandarin -->
|zh-cmn-Hans=cmn-hans<!-- still used in some old Wikimedia templates, this could be an alias of "zh-hans" on Wikimedia sites, which do not use "cmn" but assume "zh" is Mandarin -->
|zh-cmn-Hant=cmn-hant<!-- still used in some old Wikimedia templates, this could be an alias of "zh-hant" on Wikimedia sites, which do not use "cmn" but assume "zh" is Mandarin -->
|zh-gan=gan<!-- still used in some old Wikimedia templates -->
|zh-wuu=wuu<!-- still used in some old Wikimedia templates -->
|zh-yue=yue<!-- still used in some old Wikimedia templates and in interwikis -->

<!-- other "inherited" tags of the standard, strongly discouraged as they are deleted, but with no defined replacement there are left unaffected (complete list that should not be augmented now)-->
|cel-gaulish=xtg<!--ambiguous, most often "xtg" for Transalpine Gaulish in today's France, may also be "xcg" for Cisalpine Gaulish in today's Northern Italy-->
|i-enochian=x-enochian?
|i-mingo=x-mingo?

<!-- other standard "redundant" tags, which were unnecessarily registered (they validate with standard subtags) and that are left unaffected (complete list that should not be augmented now)
|az-arab
|az-cyrl
|az-latn
|be-latn
|bs-cyrl
|bs-latn
|de-1901
|de-1996
|de-at-1901
|de-at-1996
|de-ch-1901
|de-ch-1996
|de-de-1901
|de-de-1996
|en-boont
|en-scouse
|iu-cans
|iu-latn
|mn-cyrl
|mn-mong
|sl-nedis
|sl-rozaj
|sr-cyrl
|sr-latn
|tg-arab
|tg-cyrl
|uz-cyrl
|uz-latn
|yi-latn
|zh-hans
|zh-hans-cn
|zh-hans-hk
|zh-hans-mo
|zh-hans-sg
|zh-hans-tw
|zh-hant
|zh-hant-cn
|zh-hant-hk
|zh-hant-mo
|zh-hant-sg
|zh-hant-tw
  --- standard special codes
|mul
|und
  --- all other unaffected tags:
  Minimal check of validity (valid BCP47 codes are necessarily stable over URLENCODE and #titleparts).
  The check ensures that the code contains only ASCII letters, digits or hyphens, and starts by a letter.
  This check is necessary to avoid a severe bug in MediaWiki, with some values of parameter 1, notably with
  urlencoded characters (including quotes, braces, ampersands...), slashes, or any HTML or wiki formatting
  (see also [[Template:CURRENTCONTENTLANGUAGE]]). If successful, force result to lowercase; otherwise
  return an empty language tag.
  -->
|#default =
  {{#ifeq: {{#titleparts:{{{1|}}}|1}} | {{#titleparts:{{{1|}}}||-1}}
  | {{#ifeq: {{lc:{{#titleparts:{{{1|}}}|1}}}} | {{ucfirst:{{lc:{{#titleparts:{{{1|}}}|1}}}}}}
    |
    | {{#ifeq: {{{1|}}} | {{urlencode:{{{1|}}}}}
      | {{lc:{{{1|}}}}}
      }}
    }}
  }}
}}
