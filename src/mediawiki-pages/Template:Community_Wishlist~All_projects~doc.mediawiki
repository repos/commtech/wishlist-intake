{{documentation subpage}}

This template is used solely to house translations for the "All projects" message.

To view and edit all interface message translations, see [[Community Wishlist/Translation]].
<includeonly>
<!-- PUT HERE THE CATEGORIES OF THE TEMPLATE -->
[[Category:Community Wishlist/Messages{{#translation:}}]]
</includeonly>
