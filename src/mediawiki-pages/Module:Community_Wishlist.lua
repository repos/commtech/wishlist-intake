-- Utilities for Community Wishlist

local p = {}

local function getProjectName( frame, value )
	local projectCodes = {
		['wikipedia'] = 'project-localized-name-group-wikipedia',
		['wikidata'] = 'project-localized-name-wikidatawiki',
		['commons'] = 'project-localized-name-commonswiki',
		['wikisource'] = 'project-localized-name-group-wikisource',
		['wiktionary'] = 'project-localized-name-group-wiktionary',
		['wikivoyage'] = 'project-localized-name-group-wikivoyage',
		['wikiquote'] = 'project-localized-name-group-wikiquote',
		['wikiversity'] = 'project-localized-name-group-wikiversity',
		['wikifunctions'] = 'project-localized-name-wikifunctionswiki',
		['wikispecies'] = 'project-localized-name-specieswiki',
		['wikinews'] = 'project-localized-name-group-wikinews',
		['metawiki'] = 'project-localized-name-metawiki',
		['wmcs'] = 'wikimedia-otherprojects-cloudservices',
	}
	if projectCodes[ value ] == nil then
		-- For an unknown project name, show error and add to tracking category.
		return '<span class="error">Unknown: "' .. value .. '"</span>'
			.. '[[Category:Community Wishlist/Wishes with unknown project names]]'
	end
	return mw.message.new( projectCodes[ value ] ):plain()
end

function p.csvToProjectNames( frame )
	local csv = mw.text.trim( frame.args[ 1 ] )
	if csv == '' then
		return ''
	end
	local list = mw.text.split( csv, ",", true )
	local out = {}
	for _, value in ipairs( list ) do
		local project = mw.text.trim( value )
		-- Return early if one of the values is 'all'.
		if project == 'all' then
			return frame:expandTemplate{ title = 'Community Wishlist/All projects' }
		end
		table.insert( out, getProjectName( frame, project ) )
	end
	local listSep = mw.message.new( 'comma-separator' ):plain() .. mw.message.new( 'word-separator' ):plain();
	return table.concat( out, listSep )
end

function p.csvToPhabLinks( frame )
	local csv = mw.text.trim( frame.args[ 1 ] )
	if csv == '' then
		return ''
	end
	local list = mw.text.split( csv, ",", true )
	local out = {}
	for _, id in ipairs( list ) do
		table.insert( out, '[[phab:' .. id .. '|' .. id .. ']]' )
	end
	return '<span class="translate-no">'
		.. table.concat( out, mw.message.new( 'comma-separator' ):plain() .. mw.message.new( 'word-separator' ):plain() )
		.. '</span>'
end

-- Get a formatted timestamp of when the given wish or focus area was last updated
function p.lastUpdated( frame )
	local pageType = frame.args[ 1 ] or 'Wishes'
	local slug = frame.args[ 2 ]
	local titleStr = 'Community Wishlist/' .. pageType .. '/' .. slug
	local lastUpdated = frame:callParserFunction( 'REVISIONTIMESTAMP', titleStr )
	return frame:callParserFunction( '#time', 'H:i, j F Y', lastUpdated ) .. ' (UTC)'
end

-- Whether to hide a given wish index table column on the current page.
-- This is called from {{Community Wishlist/Wishes/Row}}.
-- @param frame.args.1 The name of the column, either 'area' or 'status'.
-- @return string Non-empty string if the given column should be hidden
function p.hideWishIndexCol( frame )
	local colName = frame.args[ 1 ] or ''
	local titleParts = mw.text.split( mw.title.getCurrentTitle().text, '/' )
	if titleParts[ 2 ] ~= nil and (
		( colName == 'area' and titleParts[ 2 ] == 'Focus areas' )
		or  ( colName == 'status' and titleParts[ 2 ] == 'Archive' )
	) then
		return '1'
	else
		return ''
	end
end

-- --
-- Expand a newline-separated list of focus area slugs
-- into a set of template expansions of the focus areas' cards.
--
-- @param frame.args.list The newline-separated list of focus area slugs.
function p.focusAreas( frame )
	if frame.args.list == nil then
		return ''
	end
	local lines = mw.text.split( frame.args.list, '\n' )
	local out = ''
	for _lineNum, slug in pairs( lines ) do
		local faTitle =  mw.title.new( 'Community Wishlist/Focus areas/' .. slug )
		if faTitle ~= nil then
			if faTitle.exists then
				out = out .. mw.getCurrentFrame():expandTemplate{
					-- Prepend the colon as we're transcluding from mainspace.
					title = ':' .. faTitle.prefixedText,
					args = { format = 'card' }
				}
			else
				-- All focus areas should exist before being added to a list, but if one doesn't then show a link to create it.
				local editUrl = faTitle:fullUrl( { action='edit', preload='Template:Community_Wishlist/Focus_area/preload' } )
				out = out .. '<span class="cdx-card">[' .. editUrl .. ' Create "' .. slug .. '"]</span>'
			end
		end
	end
	return out
end

-- --
-- Get a link for the title column of a table row in {{Community Wishlist/Wishes/Row}},
-- wrapped with a lang="" attribute if it's not in the current page language.
--
function p.wishRowTitle( frame )
	local lang = frame.args.lang
	if lang == '' then
		lang = frame.args.baselang
	end
	local isDiffLang = frame.args.baselang ~= '' and ( frame.args.lang == '' or frame.args.baselang ~= frame.args.lang )
	local out = '[[' .. frame.args.page .. '|' .. frame.args.title .. ']]'
	local pageContentLang = mw.title.getCurrentTitle().pageLang
	if lang ~= '' and lang ~= pageContentLang:getCode() then
		out = mw.html.create( 'span' )
			:attr( 'lang', lang )
			:wikitext( out )
	end
	return out
end

return p
