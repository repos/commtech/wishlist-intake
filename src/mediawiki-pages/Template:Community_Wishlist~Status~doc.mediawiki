<noinclude>{{Documentation subpage}}</noinclude>

Displays a status chip of one of the known statuses:

* <code>draft</code>: {{Community Wishlist/Status|draft}}
* <code>submitted</code>: {{Community Wishlist/Status|submitted}}
* <code>open</code>: {{Community Wishlist/Status|open}}
* <code>started</code>: {{Community Wishlist/Status|started}}
* <code>delivered</code>: {{Community Wishlist/Status|delivered}}
* <code>blocked</code>: {{Community Wishlist/Status|blocked}}
* <code>archived</code>: {{Community Wishlist/Status|archived}}

For any other value, it shows 'Unknown':

* <code>bogus</code>: {{Community Wishlist/Status|bogus}}

<templatedata>
{
	"params": {
		"1": {
			"label": "Status",
			"type": "string",
			"suggestedvalues": [
				"draft",
				"submitted",
				"open",
				"started",
				"delivered",
				"blocked",
				"archived"
			],
			"required": true
		}
	},
	"paramOrder": [
		"1"
	],
	"description": "Display the status of a wish"
}
</templatedata>

[[Category:Community Wishlist/Intake]]

<includeonly>
<!-- PUT HERE THE CATEGORIES OF THE TEMPLATE -->
[[Category:Community Wishlist/Messages{{#translation:}}]]
</includeonly>
