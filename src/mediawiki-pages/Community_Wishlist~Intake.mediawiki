<languages/>
<div class="user-show">
<big>''<translate><!--T:1--> Loading intake form…</translate>''</big>

<translate><!--T:2--> If the form does not load, please ensure the "<tvar name=1>WishlistIntake</tvar>" gadget is enabled in your [[<tvar name=2>Special:Preferences#mw-prefsection-gadgets</tvar>|preferences]].</translate>

<!-- to be removed in prod -->
If you're contributing to WishlistIntake on a local environment,
make sure you have the gadget deployed (`npm run deploy`)
and have your server running (<code>npm run server</code>).
See the [https://gitlab.wikimedia.org/repos/commtech/wishlist-intake/-/blob/main/README.md#contributing README] for more information.
</div>
<div class="anonymous-show">
<big><translate><!--T:3--> Please log in to submit a wish.</translate></big>

<span class="plainlinks">
[{{fullurl:Special:UserLogin|returnto={{FULLPAGENAMEE}}}} <span class="cdx-button cdx-button--fake-button cdx-button--fake-button--enabled cdx-button--size-large cdx-button--framed cdx-button--action-progressive cdx-button--weight-primary" role="button">{{int:pt-login}}</span>]
</span>
[[Category:Community Wishlist{{#translation:}}]]
[[Category:Community Wishlist/Intake]]<!-- pages that WishlistIntake acts on; do not use <nowiki>{{translation:}}</nowiki> -->
