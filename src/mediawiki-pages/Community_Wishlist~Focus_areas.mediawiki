<languages/><templatestyles src="Template:Community Wishlist/styles.css" />
< [[Special:MyLanguage/Community Wishlist|<translate><!--T:1--> Wishlist dashboard</translate>]]

<translate><!--T:2--> These are all the current active focus areas.</translate>
<translate><!--T:3--> You can learn more about each area, rate their impact, and join the community discussion.</translate>

{{Community Wishlist/Focus areas/All}}
