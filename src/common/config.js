// Use deprecated "assert" keyword to import JSON on Node 18.17.0. This also
// works in rollup.
//
// This needs to be in a separate file because eslint can't parse it. Once node
// is upgraded, this file can be removed and the things that use it can import
// config.json directly using the "with" keyword.
import config from '../../config/config.json' assert { type: 'json' };
export default config;
