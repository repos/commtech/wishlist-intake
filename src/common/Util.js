import config from './config.js';
import WishlistTemplate from './WishlistTemplate.js';

/**
 * Utility functions for the gadget and bot.
 */
export default class Util {
	/**
	 * Get the wish template object.
	 *
	 * @return {WishlistTemplate}
	 */
	static getWishTemplate() {
		return new WishlistTemplate( config.wishTemplate );
	}
}
