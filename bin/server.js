/**
 * Based on https://github.com/wikimedia-gadgets/twinkle/blob/master/scripts/server.js (CC BY-SA 3.0)
 *
 * Starts a local server, so you can test your code by importing the src/ directory from your local.
 * To use, run "npm run server", then in your https://meta.wikimedia.org/wiki/Special:MyPage/global.js
 * or local equivalent at [[Special:MyPage/common.js]], add the following:
 *   mw.loader.load('http://localhost:5501/src/WishlistIntake.js');
 */

import http from 'http';
import fs from 'fs';

const server = http.createServer( ( request, response ) => {
	const filePath = `.${ request.url }`;
	let contentType;
	if ( request.url.endsWith( '.js' ) ) {
		contentType = 'text/javascript';
	} else if ( request.url.endsWith( '.css' ) ) {
		contentType = 'text/css';
	} else {
		contentType = 'text/plain';
	}
	fs.readFile( filePath, ( error, content ) => {
		if ( error ) {
			response.end( `Oops, something went wrong: ${ error.code } ..\n` );
		} else {
			response.writeHead( 200, { 'Content-Type': contentType } );
			response.end( content, 'utf-8' );
		}
	} );
} );

const hostname = '127.0.0.1';
const port = isNaN( Number( process.argv[ 2 ] ) ) ? 5501 : process.argv[ 2 ];

server.listen( port, hostname, () => {
	console.log( `Server running at http://${ hostname }:${ port }/ ` );
} );
