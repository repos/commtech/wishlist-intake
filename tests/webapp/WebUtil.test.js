import { jest, expect, it, describe } from '@jest/globals';
import WebUtil from '../../src/webapp/WebUtil.js';
import config from '../../src/common/config.js';

const testCases = [
	{
		title: 'creating a new wish',
		wgAction: 'view',
		wgPageName: config.wishIntakePage,
		wgCategories: [],
		wgDiffOldId: null,
		paramValue: null,
		expectations: {
			isNewWish: true,
			isWishView: false,
			isWishEdit: false,
			isManualWishEdit: false,
			isWishRelatedPage: true,
			shouldShowForm: true,
			slug: ''
		}
	},
	{
		title: 'creating a new wish in Spanish',
		wgAction: 'view',
		wgPageName: config.wishIntakePage + '/es',
		wgCategories: [],
		wgDiffOldId: null,
		paramValue: null,
		expectations: {
			isNewWish: true,
			isWishView: false,
			isWishEdit: false,
			isManualWishEdit: false,
			isWishRelatedPage: true,
			shouldShowForm: true,
			slug: ''
		}
	},
	{
		title: 'viewing a wish',
		wgAction: 'view',
		wgPageName: config.wishPagePrefix + 'Example_wish',
		wgCategories: [ config.wishCategory ],
		wgDiffOldId: null,
		paramValue: null,
		expectations: {
			isNewWish: false,
			isWishView: true,
			isWishEdit: false,
			isManualWishEdit: false,
			isWishRelatedPage: true,
			shouldShowForm: false,
			slug: 'Example wish'
		}
	},
	{
		title: 'editing a wish',
		wgAction: 'view',
		wgPageName: config.wishPagePrefix + 'Example_wish',
		wgCategories: [ 'Community Wishlist/Wishes' ],
		wgDiffOldId: null,
		paramValue: '1',
		expectations: {
			isNewWish: false,
			isWishView: false,
			isWishEdit: true,
			isManualWishEdit: false,
			isWishRelatedPage: true,
			shouldShowForm: true,
			slug: 'Example wish'
		}
	},
	{
		title: 'editing a wish in Spanish',
		wgAction: 'view',
		wgPageName: config.wishPagePrefix + 'Example_wish/es',
		wgCategories: [ 'Community Wishlist/Wishes' ],
		wgDiffOldId: null,
		paramValue: '1',
		expectations: {
			isNewWish: false,
			isWishView: false,
			isWishEdit: true,
			isManualWishEdit: false,
			isWishRelatedPage: true,
			shouldShowForm: true,
			slug: 'Example wish'
		}
	},
	{
		title: 'viewing revision history of intake form',
		wgAction: 'history',
		wgPageName: config.wishIntakePage,
		wgCategories: [],
		wgDiffOldId: null,
		paramValue: null,
		expectations: {
			isNewWish: false,
			isWishView: false,
			isWishEdit: false,
			isManualWishEdit: false,
			isWishRelatedPage: false,
			shouldShowForm: false,
			slug: null
		}
	},
	{
		title: 'manually editing a wish',
		wgAction: 'edit',
		wgPageName: config.wishPagePrefix + 'Example_wish',
		wgCategories: [],
		wgDiffOldId: null,
		paramValue: null,
		expectations: {
			isNewWish: false,
			isWishView: false,
			isWishEdit: false,
			isManualWishEdit: true,
			isWishRelatedPage: true,
			shouldShowForm: false,
			slug: 'Example wish'
		}
	},
	{
		title: 'viewing diff of wish edit page',
		wgAction: 'view',
		wgPageName: config.wishIntakePage,
		wgCategories: [],
		wgDiffOldId: 123,
		paramValue: null,
		expectations: {
			isNewWish: false,
			isWishView: false,
			isWishEdit: false,
			isManualWishEdit: false,
			isWishRelatedPage: false,
			shouldShowForm: false,
			slug: null
		}
	}
];

describe( 'WebUtil', () => {
	it.each( testCases )(
		'WebUtil ($title)',
		( { wgAction, wgPageName, wgCategories, wgDiffOldId, paramValue, expectations } ) => {
			// Mock known mw function calls.
			mw.config.get = jest.fn().mockImplementation( ( key ) => {
				if ( key === 'wgPageName' ) {
					return wgPageName;
				} else if ( key === 'wgCategories' ) {
					return wgCategories;
				} else if ( key === 'wgAction' ) {
					return wgAction;
				} else if ( key === 'wgDiffOldId' ) {
					return wgDiffOldId;
				}
				throw new Error( 'Unexpected key: ' + key );
			} );
			mw.util.getParamValue = jest.fn().mockImplementation( () => paramValue );

			// Assert expectations.
			expect( WebUtil.isNewWish() ).toStrictEqual( expectations.isNewWish );
			expect( WebUtil.isWishView() ).toStrictEqual( expectations.isWishView );
			expect( WebUtil.isWishEdit() ).toStrictEqual( expectations.isWishEdit );
			expect( WebUtil.isManualWishEdit() ).toStrictEqual( expectations.isManualWishEdit );
			expect( WebUtil.isWishRelatedPage() ).toStrictEqual( expectations.isWishRelatedPage );
			expect( WebUtil.shouldShowForm() ).toStrictEqual( expectations.shouldShowForm );
			expect( WebUtil.getWishSlug() ).toStrictEqual( expectations.slug );
		}
	);
} );
