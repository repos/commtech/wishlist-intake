'use strict';

let credentials = {
	server: process.env.MW_SERVER,
	scriptPath: process.env.MW_SCRIPT_PATH,
	username: process.env.MEDIAWIKI_USER,
	password: process.env.MEDIAWIKI_PASSWORD
};

// If any of the credentials are missing, try to load them all from config/credentials.json
if ( Object.values( credentials ).filter( Boolean ).length !== credentials.length ) {
	const error = new Error(
		"Credentials missing and no credentials for 'test' found in config/credentials.json. See README for more info."
	);
	try {
		// This won't be present in CI and ESLint will complain about it.
		credentials = require( '../../config/credentials.json' ).test;
	} catch ( _e ) {
		// File is probably missing.
		throw error;
	}
	if ( !credentials ) {
		throw error;
	}
	process.env.MW_SERVER = credentials.server;
	process.env.MW_SCRIPT_PATH = credentials.scriptPath;
	process.env.MEDIAWIKI_USER = credentials.username;
	process.env.MEDIAWIKI_PASSWORD = credentials.password;
}

const { config } = require( 'wdio-mediawiki/wdio-defaults.conf' );
exports.config = { ...config,
	// Override, or add to, the setting from wdio-mediawiki.
	// Learn more at https://webdriver.io/docs/configurationfile/
	//
	// Example:
	// logLevel: 'info',
	maxInstances: 4,
	mwUser: credentials.username,
	mwPwd: credentials.password,
	baseUrl: credentials.server + credentials.scriptPath
};
