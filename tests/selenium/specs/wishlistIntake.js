import assert from 'assert';
import EditWishPage from '../pageobjects/editwish.page.js';
import ViewWishPage from '../pageobjects/viewwish.page.js';
import LoginPage from 'wdio-mediawiki/LoginPage.js';
import Util from 'wdio-mediawiki/Util.js';
import Api from 'wdio-mediawiki/Api.js';
import config from '../../../src/common/config.js';

let title;

describe( 'WishlistIntake wish submission', () => {
	before( () => {
		title = Util.getTestString( 'Selenium test wish' );
	} );

	it( 'should prompt logged out users to login', async () => {
		await EditWishPage.openForEditing( config.wishIntakePage );
		assert( EditWishPage.anonText.isDisplayed(), 'Anonymous text is not displayed' );
	} );

	it( 'should show the form with VisualEditor when browsing to the intake form', async () => {
		await LoginPage.loginAdmin();
		await EditWishPage.openForEditing( 'Community Wishlist/Intake' );
		assert( EditWishPage.titleInput.isDisplayed() );
		assert( EditWishPage.descriptionInput.isDisplayed() );
	} );

	it( 'should show errors when submitting an incomplete form', async () => {
		await EditWishPage.submitButton.click();
		assert( EditWishPage.titleError.isDisplayed() );
		assert( EditWishPage.descriptionError.isDisplayed() );
		assert( EditWishPage.typeError.isDisplayed() );
		assert( EditWishPage.projectsError.isDisplayed() );
		assert( EditWishPage.audienceError.isDisplayed() );
	} );

	it( 'should not show an error if a title is over 100 chars because of translate tags', async () => {
		await EditWishPage.titleInput.setValue( 'Lorem ipsum this is 103 characters long and test test test test test test test test test test test test' );
		await EditWishPage.submitButton.click();
		assert( EditWishPage.titleError.waitForDisplayed() );
		await EditWishPage.titleInput.setValue( 'Lorem ipsum this is 97 characters long and test test test test test test test test test test test' );
		await EditWishPage.submitButton.click();
		assert( EditWishPage.titleError.waitForDisplayed( { reverse: true } ) );
		await EditWishPage.titleInput.setValue( '<translate><!--T:1--> Lorem ipsum this is 97 characters long and test test test test test test test test test test test</translate>' );
		await EditWishPage.submitButton.click();
		assert( EditWishPage.titleError.waitForDisplayed( { reverse: true } ) );
	} );

	it( 'should reveal all projects and the "It\'s something else" field when the "All projects" checkbox is checked', async () => {
		await EditWishPage.allProjectsCheckbox.click();
		assert( EditWishPage.projectsError.waitForDisplayed( { reverse: true } ) );
		assert( EditWishPage.otherProjectInput.isDisplayed() );
	} );

	it( 'should hide errors if all required fields are filled in on submission', async () => {
		await EditWishPage.titleInput.setValue( title );
		await EditWishPage.descriptionInput.click();
		await EditWishPage.descriptionInput.setValue( 'This is a test description.\n'.repeat( 10 ) );
		await EditWishPage.firstWishTypeInput.click();
		await EditWishPage.audienceInput.setValue( 'This is a test audience' );
		await EditWishPage.phabricatorTasksInput.setValue( 'T123,T456' );
		await EditWishPage.submitButton.waitForClickable();
		await EditWishPage.submitButton.click();
		assert( await EditWishPage.titleError.waitForDisplayed( { reverse: true } ) );
	} );

	it( 'should show the confirmation message and all the data entered in the form', async () => {
		assert( await ViewWishPage.successMessage.waitForDisplayed( { timeout: 8000 } ) );
		assert.strictEqual(
			await browser.execute( () => mw.config.get( 'wgTitle' ) ),
			config.wishPagePrefix + title
		);
		assert.strictEqual( ( await ViewWishPage.wishTitle.getText() ).trim(), title );
		assert.strictEqual( await ViewWishPage.statusChip.getText(), 'Submitted' );
		assert( ( await ViewWishPage.description.getText() ).includes( 'This is a test description.' ) );
		assert.strictEqual( await ViewWishPage.wishType.getText(), 'Feature request' );
		assert.strictEqual( await ViewWishPage.projects.getText(), 'All projects' );
		assert.strictEqual( ( await ViewWishPage.audience.getText() ).trim(), 'This is a test audience' );
	} );

	after( async () => {
		const bot = await Api.bot();
		bot.delete( config.wishPagePrefix + title, 'Test cleanup' ).catch( ( e ) => {
			console.error( e );
		} );
	} );
} );
