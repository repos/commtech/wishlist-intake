import { expect, it, describe } from '@jest/globals';
import Wish from '../../src/common/Wish.js';

describe( 'Wish', () => {
	it( 'should normalize a storage CSV to an array', () => {
		expect( Wish.getArrayFromValue( 'a, b,c,,a' ) ).toStrictEqual( [ 'a', 'b', 'c' ] );
	} );

	it( 'should normalize an array to a storage CSV', () => {
		expect( Wish.getValueFromArray( [ 'a', ' b', 'c', '', 'a' ] ) )
			.toStrictEqual( 'a,b,c' );
	} );
} );
