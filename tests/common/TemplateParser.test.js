import { expect, it, describe } from '@jest/globals';
import TemplateParser from '../../src/common/TemplateParser.js';
import TemplateParserError from '../../src/common/TemplateParserError.js';

const testData = [
	{
		title: 'empty string',
		wikitext: '',
		expected: []
	},
	{
		title: 'literal chars',
		wikitext: 'hello',
		expected: []
	},
	{
		title: 'name only',
		wikitext: '{{foo}}',
		expected: [ { name: 'foo', args: {} } ]
	},
	{
		title: 'numbered argument',
		wikitext: '{{foo|bar}}',
		expected: [ { name: 'foo', args: { 1: 'bar' } } ]
	},
	{
		title: 'named argument',
		wikitext: '{{foo|bar = baz}}',
		expected: [ { name: 'foo', args: { bar: 'baz' } } ]
	},
	{
		title: 'simple',
		wikitext: '{{tpl name\n| a = b\nb\n| c = d\n}}',
		expected: [ { name: 'tpl name', args: { a: 'b\nb', c: 'd' } } ]
	},
	{
		title: 'embedded template',
		wikitext: '{{tpl name\n| a = {{foo|c=zz}}\n| c = d\n}}',
		expected: [
			{ name: 'foo', args: { c: 'zz' } },
			{ name: 'tpl name', args: { a: '{{foo|c=zz}}', c: 'd' } }
		]
	},
	{
		title: 'comment at start',
		wikitext: '<!--{{tpl1}}\n-->\n{{tpl2}}',
		expected: [ { name: 'tpl2', args: {} } ]
	},
	{
		title: 'link in template arg',
		wikitext: '{{tpl | link = [[target|text]]}}',
		expected: [ { name: 'tpl', args: { link: '[[target|text]]' } } ]
	},
	{
		title: 'nowiki',
		wikitext: '{{tpl | arg = a<nowiki>|</nowiki>b}}',
		expected: [ { name: 'tpl', args: { arg: 'a<nowiki>|</nowiki>b' } } ]
	},
	{
		title: 'unclosed template',
		wikitext: '{{foo|bar',
		expected: [],
		error: 'Syntax error parsing template at offset 9: expected "}}"'
	}
];

describe( 'TemplateParser', () => {
	it.each( testData )(
		'$title',
		( { wikitext, expected, error } ) => {
			if ( error ) {
				let exception;
				try {
					TemplateParser.parse( wikitext, () => {} );
				} catch ( e ) {
					exception = e;
				}
				expect( exception ).toBeInstanceOf( TemplateParserError );
				expect( exception.message ).toStrictEqual( error );
			} else {
				const result = [];
				TemplateParser.parse(
					wikitext,
					function ( name, args ) {
						result.push( { name: name, args: args } );
					}
				);
				expect( result ).toStrictEqual( expected );
			}
		} );
} );
