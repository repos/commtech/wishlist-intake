import { expect, it, describe } from '@jest/globals';
import WishlistTemplate from '../../src/common/WishlistTemplate.js';
import Wish from '../../src/common/Wish.js';

const template = new WishlistTemplate( 'tpl name' );

const testData = [
	{
		title: 'simple',
		wish: new Wish( {
			type: 'bug',
			title: 'Fix all the things',
			description: 'Things are broken',
			tasks: 'T1234',
			proposer: '[[User:Alice|Alice]] ([[User talk:Alice|talk]])',
			created: '10:48, 20 May 2024 (UTC)',
			projects: 'Commons,Wikidata',
			otherproject: 'Chrome extension',
			baselang: 'fr'
		} ),
		wikitext: `{{tpl name
| status =
| type = bug
| title = Fix all the things
| description = Things are broken
| audience =
| tasks = T1234
| proposer = [[User:Alice|Alice]] ([[User talk:Alice|talk]])
| created = 10:48, 20 May 2024 (UTC)
| projects = Commons,Wikidata
| otherproject = Chrome extension
| area =
| baselang = fr
}}`
	},
	{
		title: 'embedded template',
		wish: new Wish( {
			type: 'bug',
			title: 'Fix all the things',
			description: `Things are broken, for example {{tl
| title = b0rk
}}`,
			tasks: 'T1234',
			proposer: '[[User:Alice|Alice]] ([[User talk:Alice|talk]])',
			created: '10:48, 20 May 2024 (UTC)'
		} ),
		wikitext: `{{tpl name
| status =
| type = bug
| title = Fix all the things
| description = Things are broken, for example {{tl
| title = b0rk
}}
| audience =
| tasks = T1234
| proposer = [[User:Alice|Alice]] ([[User talk:Alice|talk]])
| created = 10:48, 20 May 2024 (UTC)
| projects =
| otherproject =
| area =
| baselang =
}}`
	}
];

describe( 'WishlistTemplate', () => {
	it.each( testData )(
		'$title',
		( { wish, wikitext } ) => {
			// Provided to each other.
			expect( template.getWikitext( wish ) ).toStrictEqual( wikitext );
			expect( template.getWish( wikitext ) ).toStrictEqual( wish );
			// Each provided to itself.
			expect( template.getWikitext( template.getWish( wikitext ) ) )
				.toStrictEqual( wikitext );
			expect( template.getWish( template.getWikitext( wish ) ) )
				.toStrictEqual( wish );
		} );

	it( 'invalid wikitext returns null', () => {
		expect( template.getWish( '{{tpl name\n|title=foo\ndescription=[[unclosed...|\n}}' ) )
			.toBeNull();
	} );
} );
